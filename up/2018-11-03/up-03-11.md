# Удобство и продуктивность, ч. 43

Приготовьтесь к длиннопосту, потому что в этом отчёте [Удобства и продуктивности](https://community.kde.org/Goals/Usability_&_Productivity) есть много интересного! Под катом много картинок, так что радуйтесь, что ваш интернет работает не через модем 56k (если, конечно, это не так, иначе — мои соболезнования):

## Новые возможности

Эмблемы значков Breeze [были полностью переделаны](https://phabricator.kde.org/D16421), что привело к [более полноценному виду](https://bugs.kde.org/show_bug.cgi?id=399357) и к [лучшему контрасту на фоне значков, на которых они рисуются](https://bugs.kde.org/show_bug.cgi?id=399356). Они также теперь выглядят гораздо лучше (Noah Davis, KDE Frameworks 5.52):

![0](https://phabricator.kde.org/file/data/e6y4ivic3zl3amomt5ic/PHID-FILE-mxsl2wr5iooy4zkyoicm/emblems.png)

![1](https://i.imgur.com/AQtNn6y.png)

![2](https://i.imgur.com/JvIqNyR.png)

[Также значки, относящиеся к пакетам, переделаны в похожем стиле](https://phabricator.kde.org/D16588) (Noah Davis, KDE Frameworks 5.52):

![3](https://phabricator.kde.org/file/data/6edz3xf4cuktdyo3h76a/PHID-FILE-jgfwtaozei7jksifoj2n/Screenshot_20181101_211456.png)

![4](https://phabricator.kde.org/file/data/3oe6htoglpafjludotpt/PHID-FILE-snpdnysg3lm2asdcj2vr/Screenshot_20181101_081402.png)

На странице «Обновления» в центре приложений Discover [теперь можно установить и снять выделение для всех обновлений разом](https://bugs.kde.org/show_bug.cgi?id=398231), что упростит выбор желаемых изменений (Aleix Pol Gonzalez, KDE Plasma 5.15.0)

## Исправления ошибок

Диспетчер окон KWin снова [сохраняет пользовательские параметры приложений и окон](https://bugs.kde.org/show_bug.cgi?id=399778) (Влад Загородний, KDE Plasma 5.14.3)

Discover больше не зависает на мгновение, [когда вы щёлкаете на пункт «Источники» выпадающего меню при просмотре приложения, доступного и в репозиториях дистрибутива, и в виде пакета Flatpak](https://bugs.kde.org/show_bug.cgi?id=400212) (Aleix Pol Gonzalez, KDE Plasma 5.14.3)

Диалог настройки системного лотка теперь показывает правильные значки для виджетов [«Громкость»](https://bugs.kde.org/show_bug.cgi?id=400586) и [«Сети»](https://bugs.kde.org/show_bug.cgi?id=400587) (Kai Uwe Broulik и Nate Graham, KDE Plasma 5.14.3)

Глобальные комбинации клавиш [теперь работают правильно при использовании раскладок, отличных от американской английской](https://bugs.kde.org/show_bug.cgi?id=350816) (Fabian Vogt, KDE Frameworks 5.52)

Страница «Рабочая среда» Параметров системы [теперь отображается правильно при использовании дробного коэффициента масштабирования](https://bugs.kde.org/show_bug.cgi?id=385547) (Nate Graham, KDE Plasma 5.15.0):

![5](https://i.imgur.com/V3Dj3rU.png)

В Параметрах системы название категории для глобальных комбинаций клавиш KWin больше [не изменяется случайным образом](https://bugs.kde.org/show_bug.cgi?id=400248) (David Edmundson, KDE Plasma 5.15.0)

Включение Num Lock при запуске системы [теперь работает в сеансе Wayland](https://bugs.kde.org/show_bug.cgi?id=375708) (Martin Flöser, KDE Plasma 5.15.0)

[Теперь распознаётся больше папок общего доступа Samba](https://phabricator.kde.org/D16464) (Stefan Brüns, KDE Frameworks 5.52)

HTML-сообщения в почтовом клиенте KMail [теперь читаемы при использовании тёмной цветовой схемы](https://bugs.kde.org/show_bug.cgi?id=317803) (Laurent Montel, KDE PIM 5.10.0)

## Улучшения пользовательского интерфейса

Просмотрщик значков Cuttlefish [теперь фокусирует клавиатуру на поле поиска при запуске](https://phabricator.kde.org/D16516) и [закрывается по нажатию сочетания клавиш Ctrl+Q](https://phabricator.kde.org/D16521) (Chris Holland, KDE Plasma 5.12.8)

[Исправлено множество проблем, связанных с темой Breeze для GTK](https://phabricator.kde.org/D16365), включая множество разногласий между тёмным и светлым вариантами, а также теперь гораздо легче будет вносить улучшения в будущем (Matthias Groß, KDE Plasma 5.15.0)

Меню запуска приложений по умолчанию [теперь отображает тонкие линии, разделяющие содержимое от заголовка и вкладок](https://phabricator.kde.org/D15206) (Nate Graham, KDE Plasma 5.15.0):

![6](https://phabricator.kde.org/file/data/mkfludmkewawn7mx4aec/PHID-FILE-bjljuwoa7geeebbk4lui/Example.png)

[Переработано расположение элементов на некоторых страницах диалога настройки виджета «Просмотр папки»](https://phabricator.kde.org/D16241) для больше соответствия общему виду (Nate Graham, KDE Plasma 5.15.0):

![7](https://phabricator.kde.org/file/data/tpylppwplyromr4au33a/PHID-FILE-2adll4dn47qkvouygqum/Desktop_icons_after.png)

Приведены к общему виду [значки](https://bugs.kde.org/show_bug.cgi?id=400424), [названия](https://phabricator.kde.org/T9941) и [расположение](https://phabricator.kde.org/T9954) нижних кнопок на многих страницах Параметров системы, что наиболее заметно для кнопок «Загрузить \[что-то\]…». К примеру:

![8](https://phabricator.kde.org/file/data/7gvhen3ee2slocytmabn/PHID-FILE-pbsywdh4bhtovguzlsev/Font_install.png)

![9](https://phabricator.kde.org/file/data/arwvks4v23tur4m5ggn3/PHID-FILE-nwlbocwsc24vry7yobpz/Colors.png)

![10](https://phabricator.kde.org/file/data/ge23krrxshzr4zglw3r7/PHID-FILE-5o42urhxxmc6k4tgjcbf/KWin_decorations.png)

Индексатор файлов Baloo теперь может читать [метки из файлов `.asf`](https://phabricator.kde.org/D16283), [метки APE из файлов `.wavpack` и `.ape`](https://phabricator.kde.org/D16282) и [метки ID3 из файлов `.wav` и `.aiff`](https://phabricator.kde.org/D16486) (Alexander Stippich, KDE Frameworks 5.52)

Эмулятор терминала Konsole [теперь изменяет значок неактивной вкладки, когда получает сигнал гудка, так что теперь вы сможете увидеть, откуда пришёл гудок](https://bugs.kde.org/show_bug.cgi?id=399140) (Andrew Smith, KDE Applications 18.12.0):

![11](https://i.imgur.com/tFdDGR4.png)

Текстовый редактор Kate [теперь может быть сконфигурирован так, чтобы завершать работу при закрытии последней вкладки](https://phabricator.kde.org/D16169), как [это теперь делает диспетчер файлов Dolphin](https://bugs.kde.org/show_bug.cgi?id=397101) (Thomas Surrel, KDE Applications 18.12.0)

На следующей неделе ваше имя может появиться в этом списке! Не знаете, с чего начать? Просто спросите! Я недавно смог помочь нескольким новым участникам, буду рад помочь и вам! Вы также можете найти информацию по ссылке <https://kde.ru/join> и выяснить, как стать частью того, что действительно имеет значение. Вам не требуется уже быть программистом. Я не был им, когда я только начал. Попробуйте, вам понравится! Мы не кусаемся!

Если мои усилия по выполнению, направлению и документации этой работы кажутся вам полезными, и вы хотели бы увидеть больше, тогда можете пожертвовать средств мне на [Patreon](https://www.patreon.com/ngraham), [Liberapay](https://liberapay.com/ngraham/donate) или [PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y279422B3LMRE).
Также рассмотрите возможность [пожертвования средств в фонд KDE e.V.](https://kde.org/ru/community/donations/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2018/11/03/this-week-in-usability-productivity-part-43/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
