# Удобство и продуктивность, ч. 85

> *19–25 августа — прим. переводчика*

[Я ещё не умер](https://www.youtube.com/watch?v=Jdf5EXo6I68)! Были предложены [новые цели сообщества](https://phabricator.kde.org/maniphest/query/xy6O1kMEPW0K/#R) KDE, голосование уже началось. Ну а пока инициатива [Удобство и продуктивность](https://community.kde.org/Goals/Usability_&_Productivity) продолжается. Мы на 85-й неделе. У нас есть кое-что вкусненькое для вас. Давайте смотреть:

## Новые возможности

[В просмотрщике изображений Gwenview теперь можно настраивать уровень качества JPEG. Стандартное значение — 90, как в утилите для создания снимков экрана Spectacle](https://bugs.kde.org/show_bug.cgi?id=277996) (Nate Graham, Gwenview 19.12.0):

![0](https://i.imgur.com/xx9Bq9B.png)

![1](https://i.imgur.com/wU50n5E.png)

[В Spectacle теперь можно включить сохранение текущего снимка экрана в буфер обмена всегда](https://phabricator.kde.org/D23162), что позволяет сохранять снимки [сразу и в файл, и в буфер обмена](https://bugs.kde.org/show_bug.cgi?id=390415) (Antonio Prcela, Spectacle 19.12.0)

[В текстовом редакторе Kate улучшена функция поиска и замены регулярных выражений, добавлен помощник по построению регулярных выражений](https://phabricator.kde.org/D23190) (Gregor Mi, Kate 19.12.0):

![2](https://i.imgur.com/c2F2skO.png)

## Исправления ошибок и улучшения производительности

Просмотрщик значков [Cuttlefish](https://bugs.kde.org/show_bug.cgi?id=411112), [Параметры системы](https://phabricator.kde.org/D23306), [среда разработки KDevelop](https://phabricator.kde.org/D23321) и [диспетчер буфера обмена (Klipper)](https://phabricator.kde.org/D23320) больше не дублируются в Discover и других центрах приложений (Nate Graham, Aleix Pol Gonzalez, Matthias Klumpp и Friedrich Kossebau, KDE Plasma 5.16.5)

Использование раздела «Стиль программ GNOME/GTK+» Параметров системы [больше не удаляет из конфигурационных файлов GTK настройки, которыми он не управляет](https://phabricator.kde.org/D21524). Это исправляет такие ошибки, как [неправильное отображение шрифтов](https://bugs.kde.org/show_bug.cgi?id=354963), [неуместное удаление вручную настроенных параметров для приложений GTK 2](https://bugs.kde.org/show_bug.cgi?id=322797) и [GTK 3](https://bugs.kde.org/show_bug.cgi?id=342320) (Михаил Золотухин, KDE Plasma 5.17.0)

Discover [больше не отображает сообщение об ошибке форматирования строки вместо номеров версий на странице обновлений](https://phabricator.kde.org/D23250) (Nate Graham, KDE Plasma 5.17.0)

Попытка перезаписать существующий файл на FTP-сервере [больше не зависает навсегда](https://bugs.kde.org/show_bug.cgi?id=409954) (Ярослав Сидловский, KDE Frameworks 5.62)

[Больше нельзя вытащить меню выпадающего списка QML из собственных рамок](https://phabricator.kde.org/D23285), это было просто глупо (Björn Feber, KDE Frameworks 5.62)

В диалоге открытия и сохранения файлов, как и в других просмотрщиках файлов, [больше нельзя пытаться безуспешно создать файл или папку поверх существующего файла](https://phabricator.kde.org/D23333) (Daan De Meyer, KDE Frameworks 5.62)

Некоторые кнопки в нескольких виджетах Plasma (например, кнопка *Настроить…* в виджете прогноза погоды) [больше не выглядят комически большими](https://bugs.kde.org/show_bug.cgi?id=399952) (Camilo Higuita, KDE Frameworks 5.62)

Выбор пункта «Закрыть текущую вкладку» в диалоговом окне эмулятора терминала Konsole «Подтвердить закрытие нескольких вкладок» [больше не приводит к падению приложения](https://bugs.kde.org/show_bug.cgi?id=410607) (Андрей Яшкин, Konsole 19.08.1)

Режим «Прямоугольная область» в Spectacle [теперь правильно работает в сеансе Wayland, когда подключены несколько мониторов](https://phabricator.kde.org/D23346) (David Redondo, Spectacle 19.08.1)

В Konsole [больше нельзя создать дублирующиеся закладки](https://bugs.kde.org/show_bug.cgi?id=408939) (Martin T. H. Sandsmark и Tomaz Canabrava, Konsole 19.12.0)

Kate [больше не падает вместо того, чтобы закрыться, когда клавиша Esc настроена на закрытие приложения](https://bugs.kde.org/show_bug.cgi?id=409856) (Christoph Cullmann, Kate 19.12.0)

## Улучшения пользовательского интерфейса

Уведомление «Сетевое соединение разорвано» [больше не засоряет историю уведомлений](https://phabricator.kde.org/D23248) (Nate Graham, KDE Plasma 5.16.5)

На странице обновлений Discover [во время загрузки списка обновлений теперь отображается индикатор выполнения](https://phabricator.kde.org/D23324) (Aleix Pol Gonzalez, KDE Plasma 5.17.0):

![3](https://i.imgur.com/VxkZEbS.png)

При выполнении обновлений в Discover и при возникновении проблем [сообщения об ошибках теперь отображаются в более привычном диалоговом окне, а не в виде крошечных быстро исчезающих встроенных уведомлений](https://bugs.kde.org/show_bug.cgi?id=411182) (Aleix Pol Gonzalez, KDE Plasma 5.17.0)

В Параметрах системы в разделе «Управление пользователями» [теперь отображается кнопка для смены или установки пароля, а не странное всегда пустое поле ввода](https://bugs.kde.org/show_bug.cgi?id=328664) (Méven Car, KDE Plasma 5.17.0):

![4](https://i.imgur.com/asOZBNr.png)

Меню, отображаемое при нажатии на кнопку меню-гамбургера в Параметрах системы, [теперь бросает тень и отображает горячие клавиши](https://phabricator.kde.org/D22896), как и все остальные меню (Björn Feber, KDE Plasma 5.17.0):

![5](https://i.imgur.com/l7AEAqZ.png)

Кнопки «ОК» и «Отмена» в диалоговых окнах Discover [теперь поддерживают управление с клавиатуры с помощью клавиш Enter и Esc](https://phabricator.kde.org/D23284) (Nate Graham, KDE Plasma 5.17.0)

Пользовательский интерфейс раздела «Энергопотребление», предоставляющий сведения о батарее и энергии, был переработан (Méven Car и Nate Graham, KDE Plasma 5.17.0):

![7](https://i.imgur.com/aa0gX83.png)

Файлы FictionBook [получили собственные значки](https://phabricator.kde.org/D23300) (Юрий Чорноиван, KDE Frameworks 5.62)

Длинные названия в [диспетчере файлов Dolphin](https://phabricator.kde.org/D19471) и [файловых диалоговых](https://phabricator.kde.org/D23201) теперь сокращаются в середине, а не в конце, так что [расширение файла всегда видно](https://bugs.kde.org/show_bug.cgi?id=404955) (Nate Graham, Dolphin 19.12.0 и KDE Frameworks 5.62)

Ручки перетаскивания в режиме «Прямоугольная область» в Spectacle [теперь намного приятнее в использовании, удобны на сенсорном экране](https://phabricator.kde.org/D23322) и [больше не становятся неуправляемыми при небольших размерах](https://bugs.kde.org/show_bug.cgi?id=371843) (Leon De Andrade, Spectacle 19.12.0):

![8](https://phabricator.kde.org/file/data/zy5hddwpf6o6wzsklzif/PHID-FILE-msdcdxbjjwxwvfm6wnp3/image.png)

На следующей неделе ваше имя может появиться в этом списке! Не знаете, с чего начать? Просто спросите! Я недавно смог помочь нескольким новым участникам, буду рад помочь и вам! Вы также можете прочитать статью по ссылке <https://kde.ru/join> и выяснить, как стать частью того, что действительно имеет значение. Вам не требуется быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования средств](https://kde.org/ru/community/donations/) в [фонд KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Максим Маршев](https://t.me/msmarshev)  
*Источник:* <https://pointieststick.com/2019/08/24/kde-usability-productivity-week-85/>  

<!-- 💡: Escape → Esc -->
<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Klipper → диспетчер буфера обмена (Klipper) -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
