# Удобство и продуктивность, ч. 41

Вышел новый отчёт работы [программы по улучшению удобства и продуктивности](https://community.kde.org/Goals/Usability_&_Productivity) KDE за последнюю неделю.

## Новые возможности

Глобальный коэффициент масштабирования [теперь используется GTK-приложениями, если он является целым числом](https://bugs.kde.org/show_bug.cgi?id=387849) (Peter Eszlari, KDE Plasma 5.15.0)

Виджет уведомления об обновлениях Discover теперь также [сообщает о возможности обновить версию дистрибутива](https://bugs.kde.org/show_bug.cgi?id=399141) (Aleix Pol Gonzalez, KDE Plasma 5.15.0)

В панели «Точки входа» в контекстном меню дисков и разделов [теперь отображается пункт «Свойства», позволяющий посмотреть всю информацию о них](https://bugs.kde.org/show_bug.cgi?id=181880) (Thomas Surrel, KDE Applications 18.12.0 и KDE Frameworks 5.52)

Встроенный в текстовый редактор Kate переключатель вкладок [теперь показывает путь до файлов, имеющих одинаковое название](https://phabricator.kde.org/D16054) (Gregor Mi, KDE Applications 18.12.0):

![0](https://phabricator.kde.org/file/data/4ggdja4i6qtfviogfgeu/PHID-FILE-ydrpivjaov44dnagbw5n/screenshot_20181008_232634.png)

Spectacle [теперь умеет нумеровать снимки экрана порядковыми номерами](https://phabricator.kde.org/D16304). Эта возможность будет использоваться по умолчанию, если поле «Имя файла», использующееся для именования новых снимков, пусто (Kyle Utecht, KDE Applications 18.12.0):

![1](https://i.imgur.com/PsKuMgF.png)

## Исправления ошибок

При запуске Discover нажатием на уведомление о наличии обновлений теперь сразу [открывается страница «Обновления»](https://phabricator.kde.org/D16152) (Kai Uwe Broulik, KDE Plasma 5.14.1)

Виджет «Сети» снова [показывает реальную скорость скачивания и отправки в графике скорости](https://phabricator.kde.org/D16269) (Jan Grulich, KDE Plasma 5.14.2)

Discover теперь использует новый более надёжный способ управления пакетами Flatpak, что [решает проблему возможного пропуска установки зависимостей](https://bugs.kde.org/show_bug.cgi?id=395923) (Aleix Pol Gonzalez, KDE Plasma 5.15.0)

Discover [больше не показывают категорию пакета, если она недоступна или не может быть определена](https://bugs.kde.org/show_bug.cgi?id=400045), например, для пакетов `.deb`, скачанных вручную (Nate Graham, KDE Plasma 5.14.2)

Древовидные списки в GTK-приложениях, запущенных в KDE Plasma, [теперь выглядят правильно](https://phabricator.kde.org/D16331) (Olli Helin, KDE Plasma 5.14.2):

![2](https://phabricator.kde.org/file/data/esjcjsisdsjiruidhkeh/PHID-FILE-pp6dmsps7sg7sz2mm6bk/47191505-94144180-d350-11e8-993b-a65ded238d0d.png)

Исправлена ошибка, которая [могла предотвратить подключение к некоторым типам общих папок Samba](https://bugs.kde.org/show_bug.cgi?id=399699) (Harald Sitter, KDE Applications 18.08.3)

Исправлена ошибка, из-за которой [значки оказывались обрезанными или не отцентрированными в Discover, Elisa и прочих приложениях, если глобальный коэффициент масштабирования не был целым числом](https://bugs.kde.org/show_bug.cgi?id=396990) (Kai Uwe Broulik, KDE Frameworks 5.52)

Индексатор файлов Baloo [больше не падает при индексации фотографий с пустым полем `Exif.Photo.FocalLength` в метаданных](https://phabricator.kde.org/D16165) (Игорь Побойко, KDE Frameworks 5.52)

Заработали закладки в Kate: [теперь закладки сохраняются при закрытии и последующем открытии документа](https://bugs.kde.org/show_bug.cgi?id=384087) (Silas Lenz, KDE Applications 18.08.3)

Когда утилита для создания снимков экрана Spectacle настроена на сохранение снимков в формате, отличном от PNG, [эта настройка теперь также учитывается в окне «Сохранить как…»](https://bugs.kde.org/show_bug.cgi?id=399773) (Nate Graham, KDE Applications 18.12.0)

Когда Spectacle используется для открытия снимка экрана в другом приложении, [файл теперь сохраняется в каталог по умолчанию, а не куда-то в `/tmp`](https://bugs.kde.org/show_bug.cgi?id=399395) (Nate Graham, KDE Applications 18.12.0)

Spectacle [теперь открывает правильную папку при нажатии Сервис → Открыть папку со снимками экрана](https://bugs.kde.org/show_bug.cgi?id=394182) (Kyle Utecht, KDE Applications 18.12.0)

Функция Kate «Быстрый переход» [теперь не показывает дубликаты](https://phabricator.kde.org/D15804) и [вернула свою прежнюю скорость](https://bugs.kde.org/show_bug.cgi?id=398900) (Tomaz Canabrava, KDE Applications 18.12.0)

Диспетчер файлов Dolphin и генератор миниатюр [больше не падают при создании миниатюр для файлов с расширениями `.eps` и `.ps`, если они на самом деле не являются файлами EPS и PostScript](https://bugs.kde.org/show_bug.cgi?id=399896) (Kai Uwe Broulik, KDE Applications 18.08.3)

Уведомления почтового клиента KMail [больше не показывают уродливые значки низкого разрешения](https://bugs.kde.org/show_bug.cgi?id=399231) (Laurent Montel, KDE PIM 5.9.3)

## Улучшения пользовательского интерфейса

Если нажать клавишу «Пробел» или Enter на экране блокировки до появления элементов управления, [они теперь просто появляются, не производя попытку входа](https://bugs.kde.org/show_bug.cgi?id=395671) (Thomas Surrel, KDE Plasma 5.14.2)

При поиске на странице «Рекомендованные» в Discover [теперь показываются только приложения; расширения показываются только при поиске в соответствующем разделе](https://bugs.kde.org/show_bug.cgi?id=399503) (Aleix Pol Gonzalez, KDE Plasma 5.15.0)

Приложения и расширения [теперь отображаются отдельными списками на странице «Обновления» в Discover](https://bugs.kde.org/show_bug.cgi?id=398078) (Aleix Pol Gonzalez, KDE Plasma 5.15.0)

При закрытии диалогового окна, у которого есть окно-родитель, [к нему теперь применяются эффекты закрытия окна](https://bugs.kde.org/show_bug.cgi?id=397448) (Влад Загородний, KDE Plasma 5.15.0):

<https://www.youtube.com/embed/4AKu3fdrnYQ>

Отображение событий в календаре KOrganizer [теперь выглядит привлекательнее](https://phabricator.kde.org/T9420) (Carl Schwan, KDE Applications 18.12.0):

![3](https://phabricator.kde.org/file/data/al5343r4c2jo6en6vury/PHID-FILE-3zkwypxu6rtht2wepiyj/korga.png)

У виджетов «Прогноз погоды» и «Комиксы» [теперь правильный значок на кнопке «Настроить…»](https://bugs.kde.org/show_bug.cgi?id=399888) (Nate Graham, KDE Plasma 5.15.0)

На следующей неделе ваше имя может появиться в этом списке! Не знаете, с чего начать? Просто спросите! Я недавно смог помочь нескольким новым участникам, буду рад помочь и вам! Вы также можете найти информацию по ссылке <https://kde.ru/join> и выяснить, как стать частью того, что действительно имеет значение. Вам не требуется уже быть программистом. Я не был им, когда я только начал. Попробуйте, вам понравится! Мы не кусаемся!

Если мои усилия по выполнению, направлению и документации этой работы кажутся вам полезными, и вы хотели бы увидеть больше, тогда можете пожертвовать средств мне на [Patreon](https://www.patreon.com/ngraham), [Liberapay](https://liberapay.com/ngraham/donate) или [PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=Y279422B3LMRE).
Также рассмотрите возможность [пожертвования средств в фонд KDE e.V.](https://kde.org/ru/community/donations/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2018/10/21/this-week-in-usability-productivity-part-41/>  

<!-- 💡: icon → значок -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: thumbnail → миниатюра -->
