# На этой неделе в KDE: запись экрана в Spectacle

Команда с головой погружена в работу над ошибками, и мы решаем проблемы направо и налево, готовясь к финальному выпуску Plasma 5.27! Уверен, в этот раз все читатели найдут для себя хотя бы одно исправление раздражавшей их ошибки! Мы хотим, чтобы это был лучший, самый стабильный и самый крутой выпуск Plasma 5, радующий всех восемь месяцев или дольше до самого выхода Plasma 6.

Разумеется, нам удалось тайком пронести и парочку новых функций (вы же нас знаете!), и среди них — реализация записи экрана в Spectacle! Смотрите сами:

## Новые возможности

Утилита для создания снимков экрана Spectacle теперь поддерживает запись видео с экрана в сеансе Wayland! (Aleix Pol Gonzalez, Spectacle 23.04. [Ссылка](https://invent.kde.org/graphics/spectacle/-/merge_requests/176)):

![0](https://pointieststick.files.wordpress.com/2023/01/screen-recording-in-spectacle.jpg)

VPN OpenConnect теперь поддерживают двухфакторную аутентификацию с использованием [SAML](https://ru.wikipedia.org/wiki/SAML) (Rahul Rameshbabu, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448153))

В обоях «Изображение дня» с сайта Unsplash теперь можно выбрать категорию «Кибермир» (David Elliott, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/328/diffs)):

![1](https://pointieststick.files.wordpress.com/2023/01/cyber.jpg)

## Улучшения пользовательского интерфейса

Когда в диспетчере файлов Dolphin открыта строка фильтра, в соответствии с запросом скрывающая часть элементов в активном представлении, выбор текущего расположения щелчком по нему на панели «Точки входа» теперь сбрасывает фильтр и показывает всё содержимое папки (Сергей Подтынный, Dolphin 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=259382))

Загруженные оформления рабочей среды теперь можно удалять непосредственно на одноимённой странице Параметров системы, не открывая диалог «Загрузить оформления…», как и на большинстве других страниц Параметров системы для настройки внешнего вида (Fushan Wen, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=391905))

Прокрутка колёсиком мыши над полосами прокрутки (не перетаскивание ползунка мышкой, а именно прокрутка) теперь работает как надо в приложениях на основе QtQuick (Иван Ткаченко, Frameworks 5.103. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=438526))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Spectacle *наконец-то* больше не запечатлевает собственное окно на снимках, сделанных без хотя бы секундной задержки из главного окна утилиты (David Redondo, Spectacle 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=376865))

При определённых обстоятельствах центр приложений Discover больше не завершает аварийно работу при каждом запуске, если каталог его кэша (`~/.cache/discover`) не пуст (Fabian Vogt, Plasma 5.24.8. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464517))

Исправлено недавнее ухудшение в сеансе Plasma Wayland, иногда приводившее к тому, что приложения GTK могли лишь один раз успешно отправить Plasma содержимое буфера обмена, после чего оно не обновлялось (David Redondo, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464509))

При использовании глобального масштабирования экрана приложения на GTK 4 больше не масштабируются дважды (Luca Bacci, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=442901))

Когда приложение просит систему не переходить в ждущий режим, Plasma больше не отключает заодно по ошибке и автоматическую блокировку экрана (Kai Uwe Broulik, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464119))

В сеансе Wayland исправлена ещё одна ошибка, из-за которой вы не могли выбрать разрешение экрана, отличное от родного разрешения вашего дисплея (Влад Загородний, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463619))

В сеансе Wayland приложения на GTK 2, свёрнутые до значка в системном лотке, теперь можно восстановить (Fushan Wen, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=448050))

Исправлено множество тонких недочётов в позиционировании виджетов на рабочем столе, поэтому они *наконец-то* больше не будут немного сдвигаться каждый раз, когда вы запускаете систему (Marco Martin, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2535))

В виджете истории уведомлений текст «Показать ещё» *наконец-то* больше никогда не накладывается на другие уведомления (Marco Martin, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=427894))

Если вы изменили размер всплывающего окна стандартного меню запуска приложений, поиск в нём больше не сбрасывает иногда его размер до исходного (Fushan Wen, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463862))

Вся система больше не будет иногда (а особенно при использовании файловой системы [Btrfs](https://en.wikipedia.org/wiki/Btrfs)) подвисать при установке или обновлении приложений Flatpak (David Redondo, Frameworks 5.103. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463353))

Исправлена куча странных, на вид случайных, проблем с буфером обмена в сеансе Plasma Wayland (David Redondo, Frameworks 5.103. [Ссылка 1](https://invent.kde.org/frameworks/kguiaddons/-/merge_requests/90), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=463199), [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=463323) и [ссылка 4](https://bugs.kde.org/show_bug.cgi?id=461903))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 9 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 10). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 51 (на прошлой неделе было 53). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 155 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-01-20&chfieldto=2023-01-27&chfieldvalue=RESOLVED&list_id=2261040&query_format=advanced&resolution=FIXED)

## Автоматизация и систематизация

Добавлены автоматические тесты для визуального компонента аватаров пользователей (`Kirigami.Avatar`), чтобы убедиться, что он всегда ведёт себя как надо при нажатии или касании (Иван Ткаченко, Frameworks 5.103. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/919))

## Как вы можете помочь

Помогите с тестированием [бета-версии Plasma 5.27](https://community.kde.org/Plasma/Live_Images#Ships_Plasma_5.27_Beta)! Отчёты об ошибках в бета-версии рассматриваются в первую очередь. Если вы разработчик, то можете помочь и непосредственно с устранением недочётов. Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/01/27/this-week-in-kde-major-bugfixing-and-screen-recording-in-spectacle/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: Places panel → панель «Точки входа» -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
<!-- 💡: location → расположение (в файловой системе) / местоположение (на карте) -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
