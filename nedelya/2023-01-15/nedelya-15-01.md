# На этой неделе в KDE: просто загляденье!

> 8–15 января, основное — прим. переводчика

В этот раз довольно много красивостей — набираем обороты в новом году! Посмотрите на всё это добро:

## Новые возможности

Строка поиска и запуска KRunner (в том числе «под капотом» других интерфейсов поиска, таких как меню запуска приложений и эффект «Обзор») теперь может конвертировать различные единицы измерения веса ткани (Nate Graham, Frameworks 6.0. [Ссылка](https://invent.kde.org/frameworks/kunitconversion/-/merge_requests/33)):

![1](https://pointieststick.files.wordpress.com/2023/01/screenshot-of-krunner-converting-between-170-grams-per-square-meter-and-5.013-ounces-per-square-yard.jpg)

## Улучшения пользовательского интерфейса

С учётом отзывов пользователей мы откатили изменение поведения утилиты для создания снимков экрана Spectacle, и теперь она снова запоминает геометрию последнего прямоугольного выделения только до закрытия, а не навсегда (Bharadwaj Raju, Spectacle 22.12.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463417))

Размер области заголовка музыкального проигрывателя Elisa теперь можно менять вручную, чтобы она занимала меньше места, или даже полностью сворачивать — для минималистичного вида (Arkadiusz Guzinski, Elisa 23.04. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/395)):

![2](https://pointieststick.files.wordpress.com/2023/01/elisa-with-collapsed-header-bar.jpg)

Представление «Часто воспроизводимые» в Elisa теперь представляет собой простой список песен, упорядоченный по числу прослушиваний, а не применяет сложную эвристику на основе времени, из-за которой порядок казался случайным. Кнопки порядка сортировки там, кстати, теперь тоже работают правильно (Jack Hill, Elisa 23.04. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=459985) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=442176))

Центр приложений Discover теперь предлагает вам выполнить глобальный поиск, когда вы ищете внутри категории, не содержащей вашего запроса (Nate Graham, Plasma 5.27. [Ссылка](https://bugs.kde.org/462809)):

![3](https://pointieststick.files.wordpress.com/2023/01/discover-main-window-showing-a-search-for-firefox-on-the-games-page-with-a-placeholder-message-saying-firefox-was-not-found-in-the-games-category-and-a-button-to-search-everywhere.png)

В диалоговом окне добавления пользовательской команды на странице «Комбинации клавиш» теперь есть кнопка для графического выбора файла сценария, чтобы вам не приходилось вручную вводить путь до него (Nate Graham, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1325)):

![4](https://pointieststick.files.wordpress.com/2023/01/custom-command.jpg)

При использовании обоев «Изображение дня» с источниками, которые могут предлагать потенциально неприемлемые изображения, теперь выводится небольшое предупреждение об этом (Fushan Wen, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=399126)):

![5](https://pointieststick.files.wordpress.com/2023/01/warning-about-nsfw-images.jpg)

В диалоге настройки системного лотка появилась строка фильтра, чтобы вам было проще найти значок, который вы хотите настроить (Иван Ткаченко, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1827)):

![6](https://pointieststick.files.wordpress.com/2023/01/system-tray-configuration-window-showing-search-field-with-discord-text-and-discords-system-tray-icon-visible-in-the-view.jpg)

Панель задач теперь по умолчанию отображается не более чем в одну строку (или столбец, в случае вертикальных панелей), так что она не будет больше неожиданно для вас комкаться в табличный макет. Разумеется, как и раньше, это настраивается, и вы можете включить сколько угодно строк (Felipe Kinoshita, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460911))

На странице «Переключение окон» Параметров системы функция «Выделить изменённые пункты» теперь подсвечивает пользовательские комбинации клавиш, а изменение этих комбинаций теперь вступает в силу только после нажатия кнопки «Применить» (Ismael Asensio, Plasma 5.27. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=459318) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=459325))

Приложения для удалённого доступа к рабочему столу, запущенные вне «песочницы», теперь имеют доступ к той же системе выбора экранов и разрешений, что и изолированные приложения; это позволит им задействовать привычный пользовательский интерфейс с бо́льшим контролем со стороны пользователя (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/144))

Элемент системного лотка «Настройка экранов» теперь по умолчанию включён и отображается на панели на ноутбуках с подключёнными внешними экранами, ускоряя доступ к их конфигурации (Nate Graham, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/kscreen/-/merge_requests/172) и [ссылка 2](https://invent.kde.org/plasma/kscreen/-/merge_requests/173)):

![7](https://pointieststick.files.wordpress.com/2023/01/kscreen-applet-in-system-tray.jpg)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Инкрементальный поиск в текущей области просмотра в диспетчере файлов Dolphin больше не переходит неожиданно в режим выделения при вводе пробела (Felix Ernst, Dolphin 22.12.2. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/486))

Просмотрщик изображений Gwenview снова всегда показывает миниатюры для поддерживаемых изображений RAW (Mirco Miranda, Gwenview 22.12.2. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463132))

Исправлен один из наиболее распространённых случайных сбоев в диспетчере окон KWin (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=372305))

Внесены различные улучшения в инфраструктуру активации уведомлений в сеансе Plasma Wayland, благодаря чему окна приложений, должным образом реализовавших протокол активации (например, мессенджера Telegram и Matrix-клиента NeoChat), теперь будут подниматься на передний план при нажатии на отправленные ими уведомления (Aleix Pol Gonzalez, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464099))

Исправлена ошибка, из-за которой KWin мог зависнуть при изменении размера некоторых окон в сеансе Wayland (Philipp Sieweck, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=462972))

Решены проблемы в Системном мониторе, из-за которых для видеокарт NVIDIA после недавнего обновления драйвера могли перестать отображаться данные (David Redondo, Plasma 5.27. [Ссылка 1](https://bugs.kde.org/show_bug.cgi?id=462512), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=461361) и [ссылка 3](https://bugs.kde.org/show_bug.cgi?id=463033))

При закрытии крышки ноутбука Plasma теперь сама отключает подсветку клавиатуры — на случай, если это не реализовано в прошивке устройства (Kai Uwe Broulik, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=444029))

Заголовки страниц в приложениях на основе библиотеки Kirigami больше не сокращаются иногда чрезмерно без видимой на то причины (Иван Ткаченко, Frameworks 5.103. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/900))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 7 ошибок в Plasma с очень высоким приоритетом (на одну больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 50 (на одну больше, чем на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 148 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-01-06&chfieldto=2023-01-13&chfieldvalue=RESOLVED&list_id=2250666&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

Исправлено недавнее ухудшение в драйвере видеокарт AMD, из-за которого Plasma могла застыть при получении уведомления до тех пор, пока на уведомление не будет наведён указатель мыши (Xaver Hugl, следующая версия Mesa. [Ссылка](https://gitlab.freedesktop.org/mesa/mesa/-/issues/7624))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/01/13/this-week-in-kde-well-just-look-at-all-these-pictures/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
