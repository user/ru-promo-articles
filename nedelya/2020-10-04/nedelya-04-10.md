# На этой неделе в KDE: эволюция Breeze началась

На этой неделе начала воплощаться в жизнь долгожданная [эволюция Breeze](https://phabricator.kde.org/T10891) — значительное обновление пользовательского интерфейса! Теперь обычные окна, всплывающие окна Plasma и уведомления имеют [чётко окрашенную и выделенную](https://invent.kde.org/plasma/breeze/-/merge_requests/2) визуально [«область инструментов» сверху](https://invent.kde.org/plasma/breeze/-/merge_requests/11). Тени окон [стали меньше для неактивных окон](https://bugs.kde.org/show_bug.cgi?id=393238), а на боковых панелях в окнах настроек [используются полностью цветные значки](https://phabricator.kde.org/T10165)!

> *28 сентября – 4 октября — прим. переводчика*

![0](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200928_140030.jpeg)

За это стоит поблагодарить, в первую очередь, Carson Black, Noah Davis, Niccolò Venerandi, Lindsay Roberts, Nate Graham и других участников KDE VDG! Следите за обновлениями — нас всё ещё ожидает большее. Всё это войдёт в Plasma 5.21, чему я очень рад! 🙂

Имейте в виду, что это довольно ранняя часть цикла разработки Plasma 5.21, поэтому перед выпуском могут быть доработки и корректировки окончательного внешнего вида.

Да, и это даже не половина проделанной работы! Было сделано ещё много интересного:

## Новые возможности

В контекстном меню панели проводника в текстовом редакторе Kate теперь [есть пункт «Открыть с помощью»](https://invent.kde.org/utilities/kate/-/merge_requests/110) (Mario Aichinger, Kate 20.12):

![1](https://pointieststick.files.wordpress.com/2020/09/kate-screen-shot-filebrowser-open-with.png)

Анализатор дискового пространства Filelight теперь может [сохранять текущее состояние в виде SVG](https://invent.kde.org/utilities/filelight/-/merge_requests/6) (Martin Tobias Holmedahl Sandsmark, Filelight 20.12):

![2](https://pointieststick.files.wordpress.com/2020/10/screenshot_20201002_103219.jpeg)

Благодаря работе над диспетчером окон KWin, виртуальная клавиатура в сеансе Wayland теперь [работает и с GTK-приложениями](https://invent.kde.org/plasma/kwin/-/merge_requests/293)! (Bhushan Shah, Plasma 5.21)

![3](https://pointieststick.files.wordpress.com/2020/09/gtk-virtual-keyboard.png)

Функция «Выделить изменённые параметры» в Параметрах системы [теперь работает и на страницах управления окнами](https://invent.kde.org/plasma/kwin/-/merge_requests/177) (Cyril Rossi, Plasma 5.21)

У музыкального проигрывателя Elisa благодаря усилиям Anubhav Choudhary, Nikunj Goyal и Carl Schwan появился [новый модный веб-сайт](https://elisa.kde.org/)!

![4](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200928_112039.jpeg)

## Исправления ошибок и улучшения производительности

Мастер импорта фотографий в просмотрщике изображений Gwenview [больше не зависает при попытке выйти из него](https://bugs.kde.org/show_bug.cgi?id=425971) (Gwenview 20.08.2)

Настройка «Перекрытие Page Up/Down» в просмотрщике документов Okular [снова работает](https://bugs.kde.org/show_bug.cgi?id=421950) (Kishore Gopalakrishnan, Okular 1.11.2)

При использовании автоматического устройства подачи документов для сканирования с помощью Skanlite [больше невозможно выбрать несколько областей захвата, поскольку эта концепция имеет смысл только при использовании планшетного сканера](https://bugs.kde.org/show_bug.cgi?id=415749) (Kåre Särs, Skanlite 20.12)

Теперь диспетчером файлов Dolphin можно [пользоваться с помощью пера для планшета Wacom](https://bugs.kde.org/show_bug.cgi?id=426582) (Steffen Hartlieb, Dolphin 20.12)

Пункт меню «Сохранить как…» в архиваторе Ark [был переименован в «Сохранить копию как…»](https://bugs.kde.org/show_bug.cgi?id=426960), чтобы отразить то, что он будет делать на самом деле (Elvis Angelaccio, Ark 20.12)

Исправлен [сбой KWin при активации края экрана мышью и последующем движении мыши в угол уже после срабатывания](https://bugs.kde.org/show_bug.cgi?id=423319) (Влад Загородний, Plasma 5.18.6)

Действие «Сменить пользователя», которое некоторое время отсутствовало в меню запуска приложений, [снова на месте](https://bugs.kde.org/show_bug.cgi?id=423526) (David Edmundson, Plasma 5.20)

Исправлен [сбой Plasma при редактировании настроек виджетов мониторинга системы](https://bugs.kde.org/show_bug.cgi?id=422229) (David Redondo, Plasma 5.20)

Исправлен необычный случай, когда Plasma [могла аварийно завершить работу при прокрутке на рабочем столе](https://bugs.kde.org/show_bug.cgi?id=427106) из-за того, что количество виртуальных рабочих столов неправильно считалось равным 1 при настоящем числе больше 1 (David Edmundson, Plasma 5.20)

Центр программ Discover [больше не отображает неправильное число пакетов для обновления при определённых обстоятельствах](https://bugs.kde.org/show_bug.cgi?id=426749) (Aleix Pol Gonzalez, Plasma 5.20)

Если отключить эффект KWin «Растворяющиеся всплывающие окна», [тени контекстных меню больше не остаются видимыми на короткое время после закрытия контекстных меню](https://bugs.kde.org/show_bug.cgi?id=425294) (Влад Загородний, Plasma 5.20)

В Wayland контекстные меню на рабочем столе и в Plasma теперь [закрываются, когда нужно](https://bugs.kde.org/show_bug.cgi?id=379635) (Влад Загородний, Plasma 5.20)

В Wayland [миниатюры окон в панели задач больше не перекрываются значком приложения](https://bugs.kde.org/show_bug.cgi?id=427076) (Nate Graham, Plasma 5.20)

В Wayland нажатие Ctrl+Alt+Esc дважды [больше не приводит к перемещению сообщения «Щёлкните по окну, чтобы принудительно закрыть его» в верхний левый угол](https://bugs.kde.org/show_bug.cgi?id=400675) экрана (Влад Загородний, Plasma 5.20)

Кнопки панели инструментов, открывающие меню, теперь [всегда отрисовывают стрелку вниз, указывающую на это, правильным цветом](https://invent.kde.org/plasma/breeze/-/merge_requests/34) (David Redondo, Plasma 5.20)

Диалоговое окно «Настройка параметров для указанных окон» [теперь переводится верно](https://bugs.kde.org/show_bug.cgi?id=427172) (Albert Astals Cid, Plasma 5.20)

Discover [больше не выдаёт ошибок перевода для обновляемых пакетов, номера версий которых по какой-либо причине отсутствуют](https://bugs.kde.org/show_bug.cgi?id=417192) (Aleix Pol Gonzalez, Plasma 5.21)

Теперь для внешних дисков [используются отдельные корзины, расположенные на этих дисках, чтобы не копировать файлы на основной раздел при их удалении](https://bugs.kde.org/show_bug.cgi?id=76380) (David Faure, Frameworks 5.75)

Открытие новых экземпляров уже открытых приложений, когда окнам приложений KDE разрешено восстанавливать своё предыдущее положение при повторном открытии, больше [не приводит к тому, что существующие окна полностью перекрываются; вместо этого их положение установит диспетчер окон](https://bugs.kde.org/show_bug.cgi?id=426725) (Nate Graham, Frameworks 5.75)

Исправлен [сбой в Discover при попытке обновить определённые дополнения из каталога store.kde.org](https://bugs.kde.org/show_bug.cgi?id=426732) (Aleix Pol Gonzalez, Frameworks 5.75)

Исправлена безмолвная ошибка в Discover [при обновлении некоторых дополнений из store.kde.org](https://bugs.kde.org/show_bug.cgi?id=427201) (Dan Leinir Turthra Jensen, Frameworks 5.75)

Отдельная область заголовка во всех виджетах Plasma [снова отображается при использовании темы Plasma «Breeze, тёмный вариант»](https://bugs.kde.org/show_bug.cgi?id=426537) (не стандартной Breeze с тёмной цветовой схемой, а именно «Breeze, тёмный вариант») (Harald Sitter и Nate Graham, Plasma 5.75)

Настройка для перехода непосредственно к указанному положению на полосе прокрутки по щелчку теперь [применяется и к прокручиваемым спискам в программах на основе QML](https://bugs.kde.org/show_bug.cgi?id=412685) (Bharadwaj Raju, Frameworks 5.75)

Главное окно инструмента перевода Lokalize теперь [правильно отображается в сеансе Wayland](https://bugs.kde.org/show_bug.cgi?id=424024) (Albert Astals Cid, Frameworks 5.75)

У всплывающих подсказок в Plasma [снова правильный внешний вид](https://bugs.kde.org/show_bug.cgi?id=424506) (надеюсь, что предшествующее ухудшение заметили только пользователи бета-версии Plasma 5.20) (Nate Graham, Frameworks 5.75)

Надписи элементов списка часовых поясов (а также других списков, использующих компонент `CheckDelegate` из QtQuickControls2) теперь [используют правильный цвет текста, когда флажок установлен](https://bugs.kde.org/show_bug.cgi?id=427022) (Nate Graham, Frameworks 5.75)

## Улучшения пользовательского интерфейса

В диалоговом окне настроек Kate [теперь используется красивая боковая панель со значками, как и в большинстве других приложений KDE](https://invent.kde.org/utilities/kate/-/merge_requests/105) (Christoph Cullmann, Kate 20.12):

![5](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200930_104327.jpeg)

Переработан внешний вид окон авторизации в [Nextcloud](https://invent.kde.org/network/kaccounts-providers/-/merge_requests/8) и [ownCloud](https://invent.kde.org/network/kaccounts-providers/-/merge_requests/10) на странице «Учётные записи в Интернете» Параметров системы (Nicolas Fella, KAccounts-Integration 20.12):

![6](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200930_103948.jpeg)

В окне настроек комбинаций клавиш Elisa [больше не отображаются пустые столбцы для несуществующих глобальных комбинаций](https://bugs.kde.org/show_bug.cgi?id=427072) (Nate Graham, Elisa 20.12):

![7](https://pointieststick.files.wordpress.com/2020/09/looks-better-man.jpeg)

Страница статуса Samba в Информации о системе [стала красивее](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/11) (Harald Sitter, Plasma 5.20):

![8](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200928_094909.jpeg)

Модуль проверки орфографии в строке поиска и запуска KRunner [теперь активирован по умолчанию](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/26). Его ключевое слово — «орфо» (Alexander Lohnau, Plasma 5.21):

![9](https://pointieststick.files.wordpress.com/2020/10/screenshot_20201002_120347.jpeg)

Боковые панели в Discover и других приложениях на основе библиотеки Kirigami [теперь больше похожи на панель в Параметрах системы, что делает интерфейс однороднее](https://invent.kde.org/frameworks/kirigami/-/merge_requests/101) (Nate Graham, Frameworks 5.75):

![10](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200928_131605.jpeg)

Анимации выделения [в системном лотке](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/298) и смены вкладок [в современном меню запуска приложений](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/91) теперь отзывчивее (Andy Salerno, Frameworks 5.75)

У форм [больше нет неказистого отступа слева в узком режиме](https://invent.kde.org/frameworks/kirigami/-/merge_requests/116) (Devin Lin, Frameworks 5.75):

![11](https://pointieststick.files.wordpress.com/2020/09/form_layouts.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно необязательно быть программистом, занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, [окажите финансовую поддержку](https://kde.org/ru/community/donations/) фонду [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Богдан Данильченко (expwez)](https://t.me/expwez)  
*Источник:* <https://pointieststick.com/2020/10/02/this-week-in-kde-breeze-evolution-work-starts-landing/>
