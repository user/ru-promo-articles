# На этих неделях в KDE: Akademy и Plasma 5.26

Последние две недели сообщество KDE активно готовилось к большой ежегодной очной конференции [Akademy](https://akademy.kde.org/2022), и она состоялась. Это прекрасная возможность пообщаться и лично обсудить важные темы, но об этом позже! Тем временем мы готовили Plasma 5.26, в которой большое внимание уделено стабильности. Ожидайте, что это станет ещё заметнее теперь, когда Akademy закончилась. Об этом тоже будет позднее!

## Новые возможности

К сообществу KDE присоединилось ещё одно приложение: [Ghostwriter](https://ghostwriter.kde.org/), текстовый редактор для Markdown. Поприветствуем его!

Архиватор Ark теперь поддерживает архивы формата ARJ (Илья Поминов, Ark 22.12. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=104404))

Текстовые редакторы Kate и KWrite теперь показывают экран приветствия при запуске без открытых файлов (Eric Armbruster и Christoph Cullmann, Kate и KWrite 22.12. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/888)):

![0](https://pointieststick.files.wordpress.com/2022/10/image.png)

*Так он выглядит в KWrite, а в Kate сейчас добавляется гораздо более продвинутый вариант!*

## Улучшения пользовательского интерфейса

В диспетчере файлов Dolphin перетаскивание папки на пустую область строки вкладок теперь открывает её в новой вкладке (Kai Uwe Broulik, Dolphin 22.12. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/457))

Анимация эффекта «Скольжение» при переключении рабочих столов по многочисленным просьбам была немного ускорена (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455450))

В уведомлении «Подключён новый экран» выбор опции «Продлить влево» теперь действительно размещает новый экран слева от текущего основного экрана (Allan Sandfield Jensen, Plasma 5.26. [Ссылка 1](https://invent.kde.org/plasma/kscreen/-/merge_requests/140) и [ссылка 2](https://invent.kde.org/plasma/kscreen/-/merge_requests/141))

На странице «Цвета» Параметров системы при выборе опции «Цвет выделения: В соответствии с выбранными обоями» предварительный просмотр теперь обновляется сразу, чтобы показать цвет, который будет использоваться (Tanbir Jishan, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456835))

Удаление приложения теперь сразу же удаляет его значок из избранного в меню запуска приложений (Alexander Lohnau, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=393380))

Обои «Изображение дня» теперь переключаются между картинками и между источниками изображений с плавной анимацией (Fushan Wen, Plasma 5.27. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/236))

Поле ввода для ответа на сообщение в виджете KDE Connect теперь встроено в сам виджет, а не появляется отдельным диалоговым окном (Bharadwaj Raju, KDE Connect 22.12. [Ссылка](https://invent.kde.org/network/kdeconnect-kde/-/merge_requests/430))

При размонтировании съёмного носителя в Dolphin значок «Извлечь» теперь превращается в индикатор занятости, чтобы вы знали, когда безопасно физически отключить устройство (Kai Uwe Broulik, Frameworks 5.100. [Ссылка](https://invent.kde.org/frameworks/kio/-/merge_requests/962)):

<https://i.imgur.com/a9szLHP.mp4?_=1>

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Применение глобальных тем, содержащих собственные макеты рабочего стола, больше не приводит в некоторых случаях к сбою Plasma и потере ваших панелей (Nicolas Fella, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=420245))

Параметры системы больше не вылетают иногда при переключении куда-либо со страницы «Thunderbolt» (David Edmundson, Plasma 5.24.7. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=439192))

Когда повторяющиеся графические сбои вынуждают диспетчер окон KWin отключить режим с графическими эффектами в сеансе X11, KWin теперь будет периодически проверять, сохраняется ли проблема, и по возможности повторно включит эффекты. Вам больше не придётся делать это вручную или и вовсе терять эффекты навсегда, если вы не знаете, как это делается (Arjen Hiemstra, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=452344))

Для подписей элементов в полноэкранном меню запуска приложений теперь выделено две строки, чтобы умещались более длинные названия (Tomáš Hnyk, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=362986))

В сеансе Plasma X11 при перезапуске KWin больше не теряется сопоставление окон с комнатами Plasma (David Edmundson, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=438312))

Исправлена проблема, из-за которой содержимое окон приложений (например, VLC, Firefox) могло перестать обновляться после продолжительного использования (Влад Загородний, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=456511))

В виджете «Сетевые подключения» сети больше не меняются местами, когда вы вводите пароль для одной из них (Иван Ткаченко, Plasma 5.26. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=389052))

Различные графики Системного монитора больше не мерцают, не запинаются и не зависают при использовании видеокарты NVIDIA (Łukasz Wojniłowicz, Frameworks 5.100. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=449005))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 8 ошибок в Plasma с очень высоким приоритетом (2 недели назад было 11). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 47 (2 недели назад было 44). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 209 ошибок было исправлено за 2 недели во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-09-23&chfieldto=2022-10-07&chfieldvalue=RESOLVED&list_id=2169971&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/10/08/these-weeks-in-kde-akademy-and-plasma-5-26/>  

<!-- 💡: GPU → видеокарта / графический ускоритель -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: System Monitor → Системный монитор -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: application launcher → меню запуска приложений -->
<!-- 💡: icon → значок -->
