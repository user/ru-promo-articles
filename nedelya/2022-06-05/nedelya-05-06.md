# На этой неделе в KDE: устраняем ошибки, но нужно больше

> 29 мая – 5 июня, основное — прим. переводчика

На этой неделе мы проделали много работы по исправлению ошибок и улучшению пользовательского интерфейса, но нам всё ещё требуется помощь для устранения проблем, найденных за время бета-тестирования Plasma 5.25. Если вы опытный разработчик, пожалуйста, обдумайте возможность исправления [одной-двух таких ошибок](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2080997&query_format=advanced&version=5.24.90&version=5.25.0&version=5.25.1&version=5.25.2&version=5.25.3&version=5.25.4&version=5.25.5)! Plasma 5.25 — это большой выпуск, но давайте не допустим того, чтобы он прославился большими ошибками.

## Исправленные «15-минутные ошибки»

Число ошибок на момент публикации: **65, вместо бывших 64**. 1 открыта повторно, и 0 разрешены.

Это вторая неделя подряд, когда число ошибок растёт. Причина в том, что основные разработчики Plasma очень заняты другими проектами. Поэтому если вы хотите, чтобы это число уменьшилось, то понадобится больше помощи от добровольцев. Чего же вы ждёте? 🙂

[Список ошибок на момент публикации](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2081000&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)

## Новые возможности

Теперь вы можете [удалять отдельные пункты из списков «Последние файлы» и «Последние расположения» в диспетчере файлов Dolphin, диалогах выбора файлов и других местах](https://bugs.kde.org/show_bug.cgi?id=447312) (Méven Car, Dolphin 22.08):

![0](https://pointieststick.files.wordpress.com/2022/06/forget-file.png)

Предварительный просмотр обоев рабочего стола стал проще: [достаточно щёлкнуть по ним левой кнопкой мыши в диалоге настроек рабочего стола, и он изменится, показывая, как будут выглядеть обои](https://bugs.kde.org/show_bug.cgi?id=403384). Конечно же, изменения будут применены только в том случае, если вы нажмёте кнопку «ОК» или «Применить» (Fushan Wen, Plasma 5.26)

Диалоги открытия и сохранения файлов [теперь позволяют при сортировке размещать скрытые объекты в конце](https://invent.kde.org/frameworks/kio/-/merge_requests/819), прямо как в Dolphin. [И когда вы отображаете скрытые файлы, они теперь выделены бледным шрифтом](https://invent.kde.org/frameworks/kio/-/merge_requests/838) – опять же как в Dolphin (Евгений Попов, Frameworks 5.95)

## Исправления ошибок и улучшения производительности

Сортировка по времени доступа в Dolphin [теперь работает правильно](https://invent.kde.org/system/dolphin/-/merge_requests/402) (Méven Car, Dolphin 22.04.2)

Глобальная комбинация клавиш Spectacle «Создать снимок окна под курсором мыши» (Meta+Ctrl+Print Screen) [теперь работает правильно, не приводя к некорректному запуску приложения и застреванию его в памяти после закрытия](https://bugs.kde.org/show_bug.cgi?id=446971) (Paul Worral, Spectacle 22.04.2)

Эмулятор терминала Konsole [стал надёжнее разбирать URL-адреса, содержащие номера портов или адреса IPv6](https://bugs.kde.org/show_bug.cgi?id=452978) (Ahmad Samir, Konsole 22.08)

Верхним уровнем области выбора файлов в музыкальном проигрывателе Elisa [теперь служит корневой каталог, а не домашний, так что вы можете использовать его для доступа к музыке вне вашего домашнего каталога](https://bugs.kde.org/show_bug.cgi?id=420895) (Роман Лебедев, Elisa 22.08)

Фоновая служба `kded` [больше не вызывает утечку клиентских соединений библиотеки XCB при изменении конфигурации экрана и, соответственно, не приведёт к тому, что вы в какой-то момент не сможете открывать новые приложения](https://bugs.kde.org/show_bug.cgi?id=453280) (Stefan Becker, Plasma 5.24.6)

Сторонние темы указателя мыши [вновь могут быть применены и удалены](https://bugs.kde.org/show_bug.cgi?id=453765) (Alexander Lohnau, Plasma 5.24.6)

Настройка минимальной задержки диспетчера окон KWin [теперь действительно работает](https://bugs.kde.org/show_bug.cgi?id=453694) (Malte Dronskowski, Plasma 5.24.6)

Когда вы синхронизируете настройки Plasma с экраном входа в систему SDDM, используя при этом цветовую схему, отличную от «Breeze, светлый вариант», [элементы пользовательского интерфейса SDDM теперь учитывают новую цветовую схему без необходимости предварительно очищать вручную кэш Plasma](https://bugs.kde.org/show_bug.cgi?id=440957) (Nate Graham, Plasma 5.24.6)

Центр приложений Discover [теперь находит приложения по их URL-адресам AppStream, когда опущен суффикс `.desktop`](https://invent.kde.org/plasma/discover/-/merge_requests/313/diffs), что, в частности, позволяет ему обрабатывать все ссылки с [apps.kde.org](https://apps.kde.org/ru/) (Antonio Rojas, Plasma 5.25)

Предварительный просмотр различных типов изображений формата RAW [вновь работает правильно](https://bugs.kde.org/show_bug.cgi?id=453480) (Alexander Lohnau, Frameworks 5.95)

Исправлена [существенная утечка памяти в сеансе Plasma Wayland](https://bugs.kde.org/show_bug.cgi?id=454590) (Méven Car, Frameworks 5.95)

Список «Все метки» в Dolphin [теперь показывает правильные названия для всех меток](https://bugs.kde.org/show_bug.cgi?id=449126) (Méven Car, Frameworks 5.95)

[Исправлена ошибка в компоненте области прокрутки из библиотеки Kirigami, приводившая к зависанию приложений, основанных на Kirigami, в особенности Discover](https://bugs.kde.org/show_bug.cgi?id=448784) (Marco Martin, Frameworks 5.95)

Анимация индикаторов выполнения и ползунков в приложениях на основе Qt Quick [стала плавнее](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/152) (Иван Ткаченко, Frameworks 5.95):

<https://i.imgur.com/7Iib6bk.mp4?_=1>

<https://i.imgur.com/UOwgGit.mp4?_=2>

## Улучшения пользовательского интерфейса

Нажатие на песню в списке воспроизведения Elisa при использовании сенсорного экрана [теперь немедленно воспроизводит её, вместо того чтобы просто выделить](https://bugs.kde.org/show_bug.cgi?id=454343). Вдобавок, [элементы списка воспроизведения становятся выше и удобнее для нажатия пальцем во время использования сенсорного экрана](https://invent.kde.org/multimedia/elisa/-/commit/74c3cb393dcce8b8a9b450c465cc59a6e599bb46) (Nate Graham, Elisa 22.08):

![3](https://pointieststick.files.wordpress.com/2022/06/taller-playlist-items.png)

Когда вы растягиваете окно Диспетчера разделов по вертикали, [текст на панели сведений больше не растягивается странным образом вместе с ним](https://invent.kde.org/system/partitionmanager/-/merge_requests/20) (Иван Ткаченко, Диспетчер разделов 22.08)

Диспетчер разделов [теперь показывает время работы диска в удобном для чтения виде](https://bugs.kde.org/show_bug.cgi?id=449386) (Иван Ткаченко, Диспетчер разделов 22.08)

Для улучшения совместимости со сторонними приложениями глобальные комбинации клавиш в Plasma, не использовавшие клавишу Meta, теперь используют её; вот некоторые новые комбинации клавиш:

* [Переключение раскладки клавиатуры: Ctrl+Alt+K → Meta+Alt+K](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/962)
* [Переключение на окно, требующее внимания: Ctrl+Alt+A → Meta+Ctrl+A](https://invent.kde.org/plasma/kwin/-/merge_requests/2481)
* [Удалить окно: Ctrl+Alt+Esc → Meta+Ctrl+Esc](https://invent.kde.org/plasma/kwin/-/merge_requests/2481)
* [Всплывающее меню автоматических действий: Ctrl+Alt+X → Meta+Ctrl+X](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1773)
* [Вручную произвести действие над содержимым буфера обмена: Ctrl+Alt+R → Meta+Ctrl+R](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1773)

Заметьте, что эти изменения применятся только при новой установке; для существующих пользователей сочетания клавиш не изменятся (Nate Graham, Plasma 5.25)

Теперь вы можете [щёлкнуть по заголовку алфавитной категории списка «Все приложения» в стандартном меню запуска приложений, чтобы открыть выбор буквы, к названиям на которую вы хотите переместиться](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/941) (Fushan Wen, Plasma 5.26):

![4](https://pointieststick.files.wordpress.com/2022/06/letter-grid.png)

Нажатие на кнопку «Отмена» в диалоге настроек рабочего стола [теперь отображает предупреждение о несохранённых изменениях в случае, если таковые имеются](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/969) (Fushan Wen, Plasma 5.26)

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Сергей Гримальский](https://t.me/user32767)  
*Источник:* <https://pointieststick.com/2022/06/03/this-week-in-kde-fixing-bugs-and-lets-fix-more/>  

<!-- 💡: Kickoff → [современное] меню запуска приложений -→
<!-- 💡: Partition Manager → Диспетчер разделов -→
<!-- 💡: applet → виджет -→
<!-- 💡: daemon → служба -→
<!-- 💡: locations → расположения -→
<!-- 💡: playlist → список воспроизведения -→
<!-- 💡: screenshot → снимок экрана -→
