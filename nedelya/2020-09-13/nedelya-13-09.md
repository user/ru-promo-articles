# На этой неделе в KDE: Akademy творит чудеса

На этой неделе мы провели виртуальную версию ежегодной конференции KDE, Akademy! Если вы её пропустили, доступна [куча видео на YouTube-канале сообщества KDE](https://www.youtube.com/c/KdeOrg/videos). Организаторы проделали поистине прекрасную работу, по веселью и продуктивности им удалось вплотную приблизиться к личному присутствию. Было принято много решений, сдвинуто с мёртвой точки проектов и согласовано вдохновляющих долгосрочных планов, выгоду от которых мы получим достаточно скоро. 🙂 Но мы не позволили изнурительной недельной конференции помешать нам улучшить ваши любимые программы!

> 7–13 сентября — прим. переводчика

## Новые возможности

У KDE [теперь есть потрясный новый модный веб-сайт для разработчиков, который рассказывает, как писать приложения с хорошей интеграцией с Plasma](https://develop.kde.org/) (Carl Schwan, уже онлайн!)

![0](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200906_142944.jpeg)

Просмотрщик документов Okular [получил опцию командной строки, позволяющую открыть документ на конкретной странице](https://bugs.kde.org/show_bug.cgi?id=406831) (пример: `okular /path/to/file.pdf#page=3`) (Michael Weghorn, Okular 1.12)

[Plasma теперь позволяет настроить предел зарядки батареи ниже 100%, для увеличения срока её жизни](https://invent.kde.org/plasma/powerdevil/-/merge_requests/5), если ваше устройство это поддерживает (Kai Uwe Broulik, Plasma 5.20)

![1](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200909_124853.jpeg)

Область просмотра текста в редакторе Kate, среде разработки KDevelop и других приложениях на основе KTextEditor [теперь следует выбранной глобальной цветовой схеме](https://kate-editor.org/post/2020/2020-09-06-kate-color-themes/)! (Christoph Cullmann, Frameworks 5.75)

![2](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200906_122117.jpeg)

## Исправления ошибок и улучшения производительности

Исправлена ошибка в утилите для создания снимков экрана Spectacle, которая [могла привести к графическим искажениям на снимке одного из экранов многомониторной конфигурации с глобальным масштабированием](https://bugs.kde.org/show_bug.cgi?id=385885) (Méven Car, Spectacle 20.08.2)

Исправлена ошибка, которая [могла иногда вызывать падение эмулятора терминала Konsole при выделении или вставке с удерживаемой клавишей Shift](https://bugs.kde.org/show_bug.cgi?id=398320) (Martin Tobias Holmedahl Sandsmark, Konsole 20.08.2)

Исправлена ошибка в диспетчере файлов Dolphin, из-за которой [перетаскивание файлов в окна Audacious и некоторых других приложений могло не работать](https://bugs.kde.org/show_bug.cgi?id=425757) (Elvis Angelaccio, Dolphin 20.08.2)

Исправлена ошибка в музыкальном проигрывателе Elisa, из-за которой [надпись «Пустой список воспроизведения» могла быть частично видна при скрытом списке воспроизведения](https://invent.kde.org/multimedia/elisa/-/merge_requests/164) (Ismael Asensio, Elisa 20.08.2)

Эффект плавной прокрутки для клавиш Page Up/Down в Okular [больше не мешает ускоренной прокрутке при удержании клавиш Page Up/Down или быстром многократном их нажатии](https://bugs.kde.org/show_bug.cgi?id=422050), а также [теперь применяется к поиску в обратном направлении](https://bugs.kde.org/show_bug.cgi?id=402259) (Kishore Gopalakrishnan, Okular 1.11.2)

Благодаря исправлению выше, плавная прокрутка в Okular была снова включена для [колеса мыши](https://bugs.kde.org/show_bug.cgi?id=425850) и [клавиш со стрелками](https://bugs.kde.org/show_bug.cgi?id=422041), поскольку она теперь не так раздражает (Nate Graham, Okular 1.11.2)

Исправлена ошибка в Okular, которая [могла вызывать визуальные артефакты при прокрутке после создания и выбора комментария](https://invent.kde.org/graphics/okular/-/merge_requests/226) (Havid Hurka, Okular 20.12)

Центр приложений Discover [стал запускаться быстрее](https://invent.kde.org/plasma/discover/-/commit/03c6b942ce08c744ebd71578b8ff041cf3b50882) (Aleix Pol Gonzalez, Plasma 5.20)

Строка поиска и запуска KRunner [больше не теряет первые несколько нажатий клавиш при вызове вводом на рабочем столе](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/148) (Piotr Henryk Dabrowski, Plasma 5.20)

KRunner [теперь запускается быстрее при использовании глобального сочетания клавиш, уменьшая вероятность потери первых нескольких введённых символов](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/255) (David Redondo, Plasma 5.20)

Были значительно [улучшены скорость и отзывчивость больших и сложных приложений на основе QML](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/28) (Marco Martin, Frameworks 5.75)

Исправлена странная ошибка, которая могла вызвать [сбой Kate при изменении размера шрифта после настройки системной цветовой схемы](https://bugs.kde.org/show_bug.cgi?id=417152) (Christoph Cullmann, Frameworks 5.75)

## Улучшения пользовательского интерфейса

Okular скоро [начнёт использовать тот же формат версий на основе даты, что и большинство приложений KDE](https://invent.kde.org/graphics/okular/-/merge_requests/270)! Это значит, что следующий крупный выпуск будет называться Okular 20.12, а не Okular 1.12 (Albert Astals Cid, Okular 20.12)

При использовании глобального меню или кнопки меню в строке заголовка, [меню Elisa теперь привычнее по структуре и организации](https://invent.kde.org/multimedia/elisa/-/merge_requests/149) (Carson Black, Elisa 20.12):

![3](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200911_225919.jpeg)

Та штука в Plasma, которую все называют «Режим редактирования», [теперь на самом деле использует это название в интерфейсе](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/83#note_100922) (Nate Graham, Plasma 5.20):

![4](https://pointieststick.files.wordpress.com/2020/09/screenshot_20200911_225343.jpeg)

Теперь можно [создавать папки на рабочем столе, используя стандартное сочетание клавиш (F10)](https://bugs.kde.org/show_bug.cgi?id=425191) (Domenico Panella, Plasma 5.20)

Все диалоги в Discover [теперь горизонтально центрированы в окне, вместо того чтобы быть центрированными только в правой области просмотра](https://bugs.kde.org/show_bug.cgi?id=426065) (Nate Graham, Plasma 5.20)

Когда вы переводите приложение в полноэкранный режим, [теперь показывается красивая анимация, как при распахивании окна](https://invent.kde.org/plasma/kwin/-/merge_requests/171) (Kai Uwe Broulik, Plasma 5.20)

Появился [небольшой разделитель между кнопками навигации и навигационной цепочкой](https://invent.kde.org/frameworks/kirigami/-/merge_requests/85) для представлений и панелей инструментов, у которых есть и то, и другое (Nate Graham, Frameworks 5.75):

![5](https://pointieststick.files.wordpress.com/2020/09/breadcrumbs-separator.jpeg)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом — занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, окажите финансовую поддержку [фонду KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2020/09/11/this-week-in-kde-akademy-makes-the-magic-happen/>  

<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
