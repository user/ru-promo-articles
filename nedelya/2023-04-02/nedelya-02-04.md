# На этой неделе в KDE: внимание к мелочам

На этой неделе многие участники на славу поработали над решением всяких небольших, но изрядно засидевшихся проблем. Прочитайте эту новость до конца, и наверняка найдёте исправление назойливого недочёта, затрагивавшего лично вас!

## Новые возможности

Приложение для сканирования Skanpage теперь позволяет настраивать его комбинации клавиш (Skanpage 23.08. [Ссылка](https://invent.kde.org/utilities/skanpage/-/merge_requests/41))

Текстовый редактор Kate теперь задействует языковой сервер языка QML при разработке на нём для Qt 6 (Magnus Groß, Kate 23.08. [Ссылка](https://invent.kde.org/utilities/kate/-/merge_requests/1172))

## Улучшения пользовательского интерфейса

Оценки аудиофайлов в музыкальном проигрывателе Elisa теперь можно выбирать и изменять с помощью клавиатуры (Иван Ткаченко, Elisa 23.08. [Ссылка](https://invent.kde.org/multimedia/elisa/-/merge_requests/431))

Графики энергопотребления в приложении Информация о системе стали выглядеть лучше при использовании тёмной цветовой схемы (Prajna Sariputra, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=447970)):

![0](https://pointieststick.files.wordpress.com/2023/03/info-center-energy-graph.png)

Центр приложений Discover больше не шлёт уведомления о доступных обновлениях, когда он уже открыт (Aleix Pol Gonzalez, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/519))

В приложении Информация о системе действия на страницах для удобства были перенесены из нижнего колонтитула в область заголовка (Oliver Beard, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/138)):

![1](https://pointieststick.files.wordpress.com/2023/03/info-center-header.png)

Уведомления от изолированных приложений Flatpak по умолчанию больше не воспроизводят звук (Nicolas Fella, Frameworks 5.105. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=457672))

Диалог «Открыть с помощью…» из портала XDG при поиске приложений теперь сопоставляет их не только по имени, но и по назначению, а также по поддерживаемым расширениям файлов и MIME-типам (Fushan Wen, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/168)):

![2](https://pointieststick.files.wordpress.com/2023/03/portal-open-app-dialog.png)

В различных приложениях на основе библиотеки Kirigami меню с взаимоисключающими пунктами теперь отображают правильный элемент управления — переключатель, а не флажок (Иван Ткаченко, Elisa 23.04 и Frameworks 5.105. [Ссылка 1](https://invent.kde.org/multimedia/elisa/-/merge_requests/427) и [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=467390)):

![3](https://pointieststick.files.wordpress.com/2023/03/radio-buttons-in-menu.jpg)

Приложения в формате Flatpak теперь тоже используют значки из набора Breeze (Alois Wohlschlager, Frameworks 5.105. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=424306))

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Исправлен распространённый печально известный сбой просмотрщика изображений Gwenview при быстром вращении изображения несколько раз подряд (Никита Карпей, Gwenview 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=439697))

Нажатие клавиши Print Screen для создания нового снимка экрана, когда главное окно утилиты Spectacle уже открыто, снова работает (Noah Davis, Spectacle 23.04. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=467118))

Файлы на Android-устройствах, доступные по протоколу `mtp:`, теперь можно не только просматривать, но и изменять (Harald Sitter, kio-extras 23.08. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=325574))

Устранена распространённая причина сбоев диспетчера окон KWin в сеансе Plasma Wayland при автоматическом потухании некоторых внешних экранов после их выключения и повторного включения пользователем (Aleix Pol Gonzalez, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466346))

Устранена причина сбоев фоновой службы `kded5` при переключении между экранами (Luca Bacci, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=466914))

Discover теперь значительно быстрее работает и отзывается, когда доступно много обновлений системы (Aleix Pol Gonzalez, Plasma 5.27.4. [Ссылка](https://invent.kde.org/plasma/discover/-/merge_requests/515))

Кнопка закрытия распахнутых окон приложений GTK, использующих тему Breeze, теперь срабатывает при щелчке по самому правому верхнему пикселю области заголовка (Fushan Wen, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=414777))

В сеансе Wayland настройка скорости прокрутки снова работает (Nate Graham, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464592))

В сеансе Wayland при смене темы оформления рабочей среды цветовая схема запущенных приложений GTK теперь обновляется сразу же, а не после их перезапуска (Fushan Wen, Plasma 5.27.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=421745))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Осталось 12 ошибок в Plasma с очень высоким приоритетом (на прошлой неделе было 13). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 48 (на прошлой неделе было 49). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 101 ошибка была исправлена на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-03-24&chfieldto=2023-03-31&chfieldvalue=RESOLVED&list_id=2322205&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

Chromium теперь совместим с протоколом Wayland `fractional-scale-v1`, что в дальнейшем позволит «родным» приложениям Wayland, использующим Chromium — например, браузеру Chrome и приложениям на основе Electron — надлежащим образом поддерживать дробные коэффициенты масштабирования в сеансе Wayland (Thomas Anderson, Chromium 113. [Ссылка](https://chromium-review.googlesource.com/c/chromium/src/+/4322839))

Пакет трёхмерного моделирования Blender тоже получил поддержку протокола Wayland `fractional-scale-v1`! (Campbell Barton, [ссылка](https://projects.blender.org/blender/blender/commit/cde99075e87032d99d986182e98d9c367b22d417))

## Как вы можете помочь

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/03/31/this-week-in-kde-its-the-little-things-that-count/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: applet → виджет -->
<!-- 💡: headerbar → панель заголовка -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
