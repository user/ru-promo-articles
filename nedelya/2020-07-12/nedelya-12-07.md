# На этой неделе в KDE: изобилие новых функций!

> 6–12 июля — прим. переводчика

Тонны новых классных функций и улучшений интерфейса прибыли на этой неделе, наряду с не менее важной кучей исправлений.

## Новые возможности

Музыкальный проигрыватель Elisa [теперь позволяет вам опционально отображать все жанры, исполнителей или альбомы в боковой панели, под другими элементами](https://invent.kde.org/multimedia/elisa/-/merge_requests/6) (Matthieu Gallien, Elisa 20.08.0):

![0](https://pointieststick.files.wordpress.com/2020/07/genres-embedded.jpeg)

![1](https://pointieststick.files.wordpress.com/2020/07/albums-embedded.jpeg)

Список воспроизведения Elisa [теперь отображает состояние проигрываемой дорожки в её строке](https://invent.kde.org/multimedia/elisa/-/merge_requests/131) (Stef Lep, Elisa 20.08.0):

![2](https://pointieststick.files.wordpress.com/2020/07/screenshot_20200707_100131.jpeg)

В эмуляторе терминала Konsole теперь есть включённая по умолчанию, но отключаемая возможность [отображать небольшое выделение для новых строк, появляющихся в области просмотра, когда проматывается вывод терминала](https://invent.kde.org/utilities/konsole/-/merge_requests/135) (Thomas Surrel, Konsole 20.08.0)

Значки системного лотка [теперь автоматически подстраиваются под высоту панели, и вы можете при желании выбрать, сколько строк или столбцов будет отображаться](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/130) (Nate Graham, Plasma 5.20):

![3](https://pointieststick.files.wordpress.com/2020/07/two-columns.jpeg)

![4](https://pointieststick.files.wordpress.com/2020/07/adaptive.jpeg)

## Исправления ошибок и улучшения производительности

Диспетчер файлов Dolphin [теперь правильно следует вашим предпочтениям относительно того, что делать с исполняемыми файлами](https://bugs.kde.org/show_bug.cgi?id=421294) (Wolfgang Bauer, Dolphin 20.04.3)

При вводе поискового запроса в Dolphin [положение курсора больше не сбрасывается при появлении результатов](https://bugs.kde.org/show_bug.cgi?id=423328) (Ismael Asensio, Dolphin 20.08.0)

Elisa получила несколько исправлений для экранов с высокой плотностью пикселей (HiDPI), относящихся к [толщине линий](https://invent.kde.org/multimedia/elisa/-/merge_requests/133) и [размеру значков](https://invent.kde.org/multimedia/elisa/-/merge_requests/134) (Nate Graham, Elisa 20.08.0)

При использовании стилуса для ввода в сеансе Wayland [положение рисуемых линий больше не сдвигается по вертикали на высоту строки заголовка](https://bugs.kde.org/show_bug.cgi?id=423833) (Aleix Pol Gonzalez, Plasma 5.19.3)

Применение оформления рабочей среды [теперь также правильно меняет цвета для GTK-приложений](https://bugs.kde.org/show_bug.cgi?id=421745) (Михаил Золотухин, Plasma 5.19.4)

Строка поиска и запуска KRunner и меню запуска приложений [снова могут быть использованы для открытия окон настроек, которые напрямую не видны в Параметрах системы, таких как настройки корзины или страница настроек темы Breeze](https://bugs.kde.org/show_bug.cgi?id=423612) (Alexander Lohnau, Plasma 5.19.4)

Стиль отображения «Только текст» новых виджетов Системного монитора [теперь работает как надо](https://bugs.kde.org/show_bug.cgi?id=423071) (Marco Martin, Plasma 5.19.4)

Исправлено [падение сеанса Wayland при выходе из сна компьютера с несколькими подключёнными мониторами](https://bugs.kde.org/show_bug.cgi?id=422460) (Andreas Haratzis, Plasma 5.20)

Исправлена ошибка, из-за которой [значки панели задач могли пропасть при изменении разрешения](https://bugs.kde.org/show_bug.cgi?id=373075) или [отображать неправильные значки, если экран выключен или отсоединён](https://bugs.kde.org/show_bug.cgi?id=394532) (Александр Кандауров, Plasma 5.20)

Проводник виджетов Plasma [теперь сообщает о наличии уже размещённого экземпляра виджета, только если этот виджет виден на текущем экране и в активной комнате](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/132) (David Redondo, Plasma 5.20)

В сеансе Wayland [теперь можно перейти в полноэкранный режим в проигрывателе MPV двойным щелчком по видео](https://bugs.kde.org/show_bug.cgi?id=421232) (Benjamin Port, Plasma 5.20.0)

Изменение параметра «Запрашивать подтверждение при выходе» [теперь работает сразу, а не после перезапуска](https://bugs.kde.org/show_bug.cgi?id=423864) (David Edmundson, Plasma 5.20.0)

Оформления окон [теперь отображаются правильно при использовании 30-битных (10 бит на цвет) экранов](https://bugs.kde.org/show_bug.cgi?id=406302) (Bernie Innocenti, Plasma 5.20)

Предварительный просмотр курсоров в реальном времени при наведении на плитку набора в Параметрах системы [теперь правильно работает в сеансе Wayland](https://bugs.kde.org/show_bug.cgi?id=424048) (David Redondo, Plasma 5.20)

Разделители меню в приложениях на основе QML [теперь имеют правильную высоту и толщину при использовании экрана с высокой плотностью пикселей и глобального масштабирования](https://bugs.kde.org/show_bug.cgi?id=423653) (Arjen Hiemstra, Frameworks 5.73):

![5](https://pointieststick.files.wordpress.com/2020/07/screenshot_20200707_094802.jpeg)

*Зоркие читатели могли заметить, что, несмотря на исправление выше, некоторые из разделителей высотой в 1 пиксель, а другие — 2. Это на данный момент неизбежный артефакт, вызванный использованием мной дробного масштабирования в сеансе X11. Ситуация уже лучше в Wayland, и я изучаю, можем ли мы что-нибудь сделать с этим и в X11, но эта подпись уже слишком длинная для дальнейших объяснений!*

Исправлен [широкий спектр падений Plasma, в частности с виджетом мониторинга температур](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/29) (David Edmundson, Frameworks 5.73)

## Улучшения пользовательского интерфейса

Программа для удалённого доступа к рабочему столу KRDC [теперь отображает правильные курсоры с VNC-сервера вместо маленькой точки с тормозным удалённым курсором рядом](https://invent.kde.org/network/krdc/-/merge_requests/2) (Олег Черновский, KRDC 20.08.0):

<https://i.imgur.com/8DNLh5w.mp4?_=1>

Выпадающий терминал Yakuake [теперь позволяет вам настроить все сочетания клавиш, которые на самом деле реализованы в Konsole](https://bugs.kde.org/show_bug.cgi?id=412496) (Maximillian Schiller, Yakuake 20.08.0)

Виджет использования дискового пространства [теперь внешне больше похож на аналогичный из Plasma 5.18 и предыдущих версий](https://bugs.kde.org/show_bug.cgi?id=421512) (но внутри всё ещё использует новый модный механизм, конечно же) (Marco Martin, Plasma 5.20):

![7](https://pointieststick.files.wordpress.com/2020/07/screenshot_20200710_092603.jpeg)

При использовании настройки «Увеличить максимальную громкость» и установленной громкости выше 100% [подпись индикатора уровня громкости меняет цвет, чтобы показать, что ваша громкость большая-пребольшая](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/8) (Nate Graham, Plasma 5.20):

![8](https://pointieststick.files.wordpress.com/2020/07/orange.jpeg)

![9](https://pointieststick.files.wordpress.com/2020/07/red.jpeg)

Устаревшей страницы «Смайлики» в Параметрах системы [больше нет](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/52) (Aleix Pol Gonzalez, Plasma 5.20)

Страница «Комбинации клавиш» Параметров системы [больше не показывает загадочных категорий, таких как «Служба KDE» или «Параметры системы», для очевидно не относящихся к ним действий, и вместо этого группирует их в новую категорию под названием «Служба пользовательских сочетаний клавиш»](https://invent.kde.org/plasma/khotkeys/-/merge_requests/2) (David Redondo, Plasma 5.20):

![10](https://pointieststick.files.wordpress.com/2020/07/screenshot_20200710_091803.png)

Список пользователей на экранах входа и блокировки [больше нельзя бесцельно перетаскивать по экрану, когда в нём всего один пользователь](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/149) (David Redondo, Plasma 5.20)

## Как вы можете помочь

Вы можете прочитать [эту статью](https://kde.ru/join) и выяснить, как стать частью того, что действительно имеет значение. Каждый участник KDE играет огромную роль; к вам не относятся как к очередному сотруднику или винтику системы. Программистом вам тоже быть не требуется. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

И наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [пожертвования](https://kde.org/ru/community/donations/) средств в фонд [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2020/07/10/this-week-in-kde-new-features-galore/>  

<!-- 💡: Kicker → классическое меню запуска приложений -->
<!-- 💡: Kickoff → [современное] меню запуска приложений -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: daemon → служба -->
<!-- 💡: icon → значок -->
<!-- 💡: playlist → список воспроизведения -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
