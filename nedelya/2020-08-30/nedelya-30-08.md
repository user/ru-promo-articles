# На этой неделе в KDE: полным-полно нововведений, исправлений и улучшений интерфейса в Plasma 5.20

Выпуск Plasma 5.20 обещает быть одним из самых крупных! Он и так бы уже был таковым, но на этой неделе мы добавили к нему ещё больше: больше возможностей, больше исправлений долгоиграющих ошибок, больше улучшений в интерфейсе пользователя! Все подробности дальше.

> 24–30 августа — прим. переводчика

## Новые возможности

Plasma [теперь предупреждает, когда ваш жёсткий диск или SSD скоро умрут, и позволяет следить за их состоянием в приложении Информация о системе](https://bugs.kde.org/show_bug.cgi?id=254313) (Harald Sitter, Plasma 5.20)

При использовании темы GTK Breeze заголовки окон GTK-приложений c оформлением на стороне клиента (CSD) [теперь используют то же оформление кнопок заголовка окна, что и другие приложения](https://invent.kde.org/plasma/kde-gtk-config/-/merge_requests/2) (Михаил Золотухин, Plasma 5.20):

<https://i.imgur.com/0aZPjlh.mp4?_=1>

Строку поиска и запуска KRunner [теперь можно настроить так, чтобы она была плавающим окном](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/132), вместо прилипания к верху экрана (Alexander Lohnau, Plasma 5.20):

![1](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200824_130427.jpeg)

*Технически, эта возможность уже была, вот только интерфейса для её включения не было, поэтому практически никто о ней не знал!*

Стало возможным [удалять установленные пользователем сценарии KWin прямо с их страницы в Параметрах системы](https://bugs.kde.org/show_bug.cgi?id=315829) (Alexander Lohnau, Plasma 5.20)

Те из вас, кто привык к поведению дока в macOS, теперь при желании могут [настроить виджет «Панель задач (только значки)» так, чтобы активное окно не сворачивалось при щелчке по нему](https://bugs.kde.org/show_bug.cgi?id=405495) (Nate Graham, Plasma 5.20):

![2](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200828_214654.jpeg)

Если вы из тех, кто считает новую функцию «Приложения KDE запоминают геометрию своих окон» вредной, вы [можете её отключить](https://invent.kde.org/plasma/kwin/-/merge_requests/211) (Nate Graham, Plasma 5.20):

![3](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200828_104622.jpeg)

*Обратите внимание, что это влияет только на окна приложений KDE; мы не можем контролировать сторонние приложения, которые реализуют собственные механизмы запоминания геометрии окон (по крайней мере, в X11; в Wayland мы сможем включать или выключать запоминание положений для всех окон)*

## Исправления и улучшения производительности

Кнопка «Сортировка» в просмотрщике изображений Gwenview [теперь работает правильно и совпадает по виду и поведению с таковой в диспетчере файлов Dolphin](https://bugs.kde.org/show_bug.cgi?id=425739) (Nate Graham, Gwenview 20.08.1)

Диспетчер разделов [больше не падает при создании одного раздела ext4 на неформатированной SD-карте](https://bugs.kde.org/show_bug.cgi?id=425097) (Adriaan de Groot, Диспетчер разделов KDE 4.2.0)

Архиватор Ark [теперь использует куда меньше памяти при распаковке файлов](https://invent.kde.org/utilities/ark/-/merge_requests/4) (Алексей Иванов, Ark 20.12.0)

Dolphin теперь [определяет доступные вашему компьютеру общие папки Samba независимо от способа его запуска](https://bugs.kde.org/show_bug.cgi?id=425678) (Harald Sitter, Dolphin 20.12.0)

Центр программ Discover [больше не помечает ошибочно как установленные элементы, которые вы установили через диалог «Загрузка материалов из интернета» в Параметрах системы, а затем удалили из самих Параметров системы](https://bugs.kde.org/show_bug.cgi?id=416255) (Alexander Lohnau, Plasma 5.20)

Глобальные комбинации клавиш для запуска сторонних приложений [снова работают](https://bugs.kde.org/show_bug.cgi?id=421329) (простите, что так долго) (Méven Car, Frameworks 5.74)

Исправлен случай, в котором [Discover мог упасть во время установки обновлений, включающих «материалы из интернета» (GHNS)](https://bugs.kde.org/show_bug.cgi?id=425811) (Dan Leinir Turthra Jensen, Frameworks 5.74)

Устранена ошибка, из-за которой [Dolphin мог аварийно завершиться при отправке файлов по Bluetooth](https://bugs.kde.org/show_bug.cgi?id=419170) (Nicolas Fella, Frameworks 5.74)

Дополнения, устанавливаемые через диалог «Загрузка материалов из интернета», [теперь устанавливаются как надо и при наличии у них в названии косой черты](https://bugs.kde.org/show_bug.cgi?id=417216) (Alexander Lohnau, Frameworks 5.74)

Эффект выделения для файлов и папок под курсором в адресных строках в различных приложениях KDE [больше не излишне широкий](https://bugs.kde.org/show_bug.cgi?id=425570) (Ismael Asensio, Frameworks 5.74)

## Улучшения пользовательского интерфейса

Диалог создания общей папки Samba [теперь автоматически отключает опцию гостевого доступа, если системные настройки Samba запрещают её](https://bugs.kde.org/show_bug.cgi?id=425203), вместо того чтобы всё равно дать вам попробовать и молча завершиться неудачей, [а также проверяет ситуацию, когда пользователь не состоит в нужной группе](https://bugs.kde.org/show_bug.cgi?id=407846) (Harald Sitter, Dolphin 20.12.0)

Утилита для создания снимков экрана Spectacle по умолчанию [больше не копирует путь к свежесохранённому снимку в буфер обмена](https://invent.kde.org/graphics/spectacle/-/merge_requests/15) (Claudius Ellsel, Spectacle 20.12.0)

Стрелки в заголовках табличного отображения [теперь смотрят в ожидаемом направлении: вниз, когда наибольшие элементы вверху, и вверх, когда внизу](https://invent.kde.org/plasma/breeze/-/merge_requests/24) (Nate Graham, Plasma 5.20):

![4](https://pointieststick.files.wordpress.com/2020/08/arrow-pointing-down-1.jpeg)

*А при сортировке по имени или другому критерию, не использующему числа/размеры/количества, стрелки бессмысленны независимо от их направления, так что это вообще не важно 🙂*

Диалоги [«Дополнения»](https://invent.kde.org/plasma/discover/-/merge_requests/27) и [«Удаляемые пакеты»](https://bugs.kde.org/show_bug.cgi?id=419341) в Discover получили визуальные улучшения:

![5](https://pointieststick.files.wordpress.com/2020/08/addons-sheet.jpeg)

![6](https://pointieststick.files.wordpress.com/2020/08/packages-to-remove-sheet.jpeg)

Страница настройки комбинаций клавиш в Параметрах системы [теперь поддерживает навигацию с клавиатуры](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/137) (Carl Schwan, Plasma 5.20)

Диалоги Kirigami [теперь используют более уместный и приятный фоновый цвет для верхних и нижних колонтитулов](https://invent.kde.org/frameworks/kirigami/-/merge_requests/86) (Nate Graham, Frameworks 5.74):

![7](https://pointieststick.files.wordpress.com/2020/08/screenshot_20200826_093128-1.jpeg)

Размеры окон приложений KDE [теперь запоминаются в зависимости от экрана](https://invent.kde.org/frameworks/kconfig/-/merge_requests/17), как и их положения (Nate Graham, Frameworks 5.74)

Файлы игрового движка Godot [получили красивые значки](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/39) (Michael Alexsander, Frameworks 5.74):

![8](./8_godot.png)

Менеджер паролей KeePassXC [снова использует красивые значки в теме Breeze](https://bugs.kde.org/show_bug.cgi?id=425928) (Nate Graham, Frameworks 5.74)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник, и совершенно не обязательно быть программистом — занятие по душе найдётся для каждого.

И наконец, если вы находите программное обеспечение KDE полезным, окажите финансовую поддержку [фонду KDE e.V.](https://ev.kde.org/)

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [boingo-00](https://t.me/boingo00)  
*Источник:* <https://pointieststick.com/2020/08/28/this-week-in-kde-tons-and-tons-and-tons-of-plasma-5-20-features-bugfixes-and-ui-improvements/>  

<!-- 💡: Info Center → Информация о системе -->
<!-- 💡: Partition Manager → Диспетчер разделов -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: headerbar → панель заголовка -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: task manager → панель задач -->
