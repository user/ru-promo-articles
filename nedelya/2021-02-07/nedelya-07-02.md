﻿# На этой неделе в KDE: прямая отрисовка полноэкранных приложений

Работа над Plasma 5.21 подходит к концу, были устранены почти все проблемы, обнаруженные на стадии бета-тестирования. Сейчас идёт работа над следующими крупными обновлениями! В их числе…

> 31 января – 7 февраля, основное — прим. переводчика

## Новые возможности

В Gwenview [стало возможным отключение функции «Вид сверху» в правом нижнем углу при увеличении изображения](https://bugs.kde.org/show_bug.cgi?id=426105) (Madhav Kanbur, Gwenview 21.04)

Диспетчер окон KWin получил поддержку [прямой отрисовки полноэкранных приложений](https://invent.kde.org/plasma/kwin/-/merge_requests/502) (например, игр) в сеансе Wayland, что повысит производительность и снизит задержки (Xaver Hugl, Plasma 5.22)

## Исправления ошибок и улучшения производительности

Gwenview [больше не проявляет визуальные артефакты при панорамировании и увеличении изображений, когда используется глобальный коэффициент масштабирования](https://invent.kde.org/graphics/gwenview/-/merge_requests/35) (Влад Загородний, Gwenview 20.12.2)

Числовое поле выбора качества JPEG в Gwenview [снова работает](https://invent.kde.org/graphics/gwenview/-/merge_requests/36) (Madhav Kanbur, Gwenview 20.12.3)

В Gwenview [задействована новая область отрисовки OpenGL, которая реализует аппаратное ускорение при переходе между изображениями в Wayland, а также исправляет различные ошибки и сбои](https://invent.kde.org/graphics/gwenview/-/merge_requests/37) (Madhav Kanbur, Gwenview 20.12.3)

Исправлено ухудшение в строке поиска и запуска KRunner, когда [приоритет при поиске файлов отдавался соответствию подстрокам из нескольких слов, а не точному совпадению с одним словом](https://bugs.kde.org/show_bug.cgi?id=431609). Также [поиск теперь в целом точнее сопоставляет](https://bugs.kde.org/show_bug.cgi?id=432339) (Harald Sitter, Plasma 5.21)

Исправлена [проблема с отрисовкой изображения на системах с несколькими графическими процессорами в пользовательском сеансе Wayland](https://bugs.kde.org/show_bug.cgi?id=431968) (Xaver Hugl, Plasma 5.21)

Исправлена [проблема в KWin с обновлением содержимого окна Firefox, запущенного в режиме «родного» клиента Wayland с определёнными специфическими параметрами](https://bugs.kde.org/show_bug.cgi?id=428499) (Влад Загородний, Plasma 5.21)

Решена [проблема с производительностью на слабых графических процессорах Intel](https://invent.kde.org/plasma/kwin/-/merge_requests/627), особенно [при прокрутке страниц в браузере Firefox](https://invent.kde.org/plasma/kwin/-/merge_requests/635) (Влад Загородний, Plasma 5.21)

Пункты меню в приложениях GTK [больше не слишком высокие](https://bugs.kde.org/show_bug.cgi?id=431819) (Jan Blackquill, Plasma 5.21)

Dolphin [больше не заканчивает свою работу аварийно, если быстро подряд пропустить копирование или перемещение нескольких файлов во время массовой операции](https://bugs.kde.org/show_bug.cgi?id=431731) (Ahmad Samir, Frameworks 5.79)

Приложения KDE, окна которых были [закрыты в распахнутом состоянии, при повторном запуске будут открываться распахнутыми. Если же после этого они будут восстановлены, при закрытии и повторном запуске они сохранят размер](https://bugs.kde.org/show_bug.cgi?id=430521) (Nate Graham, Frameworks 5.79)

## Улучшения пользовательского интерфейса

Теперь вы можете [дважды или трижды щёлкнуть по уведомлению, чтобы выделить текст, как в других текстовых представлениях](https://bugs.kde.org/show_bug.cgi?id=431398), что может быть полезно, например, для быстрого выбора и копирования одноразовых кодов, приходящих вам в уведомлениях на смартфоне и отображаемых на компьютере магией KDE Connect (Kai Uwe Broulik, Plasma 5.22)

Уведомления, связанные с файловыми операциями, теперь [показывают каталог назначения в качестве интерактивной ссылки, по которой можно сразу перейти](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/626) (Kai Uwe Broulik, Plasma 5.22):

![0](https://pointieststick.files.wordpress.com/2021/02/clickable-link-in-file-notification.png?w=776)

Анимации системного лотка теперь более пространственно согласованы. [Вид сдвигается в противоположном направлении от значка, на который вы нажали](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/586). На системном лотке в вертикальной панели будет использована анимация плавного перехода, поскольку анимация вертикального сдвига там выглядит довольно странно (Jan Blackquill, Plasma 5.22)

Значок приложения Telegram в системном лотке [теперь использует правильные цвета и соответствует цветовой схеме Plasma](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/81) (Rocket Aaron, Frameworks 5.79):

![1](https://pointieststick.files.wordpress.com/2021/02/telegram-tray-dark.png?w=528)
![2](https://pointieststick.files.wordpress.com/2021/02/telegram-tray-light.png?w=524)

Диалоги «Загрузить \[что-то\]» [теперь имеют упрощённый интерфейс сортировки и фильтрации](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/101) (Dan Leinir Turthra Jensen, Frameworks 5.79):

![3](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210205_132723.jpeg?w=1024)

Рейтинг материалов в диалогах «Загрузить \[что-то\]» [теперь отображает число, соответствующее звёздам](https://invent.kde.org/frameworks/knewstuff/-/merge_requests/92) (Dan Leinir Turthra Jensen, Frameworks 5.79):

![4](https://pointieststick.files.wordpress.com/2021/02/screenshot_20210202_083316.jpeg?w=1024)

## И ещё кое-что…

Лидер цели KDE [«Согласованность»](https://community.kde.org/Goals/Consistency) Niccolò Venerandi выпустил полезное видео, демонстрирующее основы создания тем Plasma! Посмотрите:

<https://www.youtube.com/watch?v=XrNWYt_vciA>

## Как вы можете помочь

Прочитайте эту [статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку фонду](https://kde.org/community/donations/) [KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Виталий Рябичев](https://vk.com/jistake)  
*Источник:* <https://pointieststick.com/2021/02/05/this-week-in-kde-kwin-gains-direct-scan-out-and-gwenview-gets-a-lot-of-love/>  

<!-- 💡: get new [thing] → «Загрузить \[что-то\]» -->
<!-- 💡: icon → значок -->
<!-- 💡: icons → значки -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: directory → каталог -->
