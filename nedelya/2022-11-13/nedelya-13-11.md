# На этой неделе в KDE: упрощённая настройка переменных среды

## Новые возможности

В Редакторе меню и диалоговом окне свойств упрощена установка переменных среды при открытии приложений. Это всегда было возможно, но раньше нужно было знать специальный синтаксис (`Exec=env FOO=1 kate`); теперь же это явным образом поддерживается (Dashon Wells, Frameworks 5.101 и Plasma 5.27. [Ссылка 1](https://invent.kde.org/frameworks/kio/-/merge_requests/1025) и [ссылка 2](https://invent.kde.org/plasma/kmenuedit/-/merge_requests/11)):

![0](https://pointieststick.files.wordpress.com/2022/11/image-4.png)

## Улучшения пользовательского интерфейса

Опция для отключения интерфейса Secret Service теперь объясняет, что это значит и зачем это может пригодиться (Guilherme Marçal Silva, KWalletManager 22.12. [Ссылка](https://invent.kde.org/utilities/kwalletmanager/-/merge_requests/19)):

![1](https://pointieststick.files.wordpress.com/2022/11/image-3.png)

*«Использовать KWallet для предоставления Secret Service: Интерфейс Secret Service позволяет приложениям хранить пароли и другую секретную информацию. Отключите эту функцию, если хотите использовать сторонний сервис бумажника, например, KeePassXC или Связку ключей GNOME».*

Центр приложений Discover больше не показывает категории светлым текстом на карточках приложений, так как это лишь зашумляло их. Кроме того, вернулась категория «Все приложения», которую можно использовать, чтобы ограничить поиск только приложениями (Nate Graham и Aleix Pol Gonzalez, Plasma 5.27. [Ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/411), [ссылка 2](https://bugs.kde.org/show_bug.cgi?id=461272) и [ссылка 3](https://invent.kde.org/plasma/discover/-/merge_requests/412)):

![2](https://pointieststick.files.wordpress.com/2022/11/image-5.png)

На многих прокручиваемых страницах Параметров системы разделитель над кнопками нижнего колонтитула теперь выровнен с разделителем над кнопкой «Выделить изменённые параметры» на боковой панели (Nate Graham, Frameworks 5.101. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=461435)):

![3](https://pointieststick.files.wordpress.com/2022/11/image-6.png)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

При выравнивании вертикально расположенных мониторов по правому краю в Параметрах системы автоматическая привязка больше не создаёт неприятный горизонтальный сдвиг в 1 пиксель, заметный по разрешениям снимков экрана (Александр Волков, Plasma 5.26.4. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=455394))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Известно об 11 ошибках в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 51 (на прошлой неделе было 50). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 174 ошибки были исправлены на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2022-11-04&chfieldto=2022-11-11&chfieldvalue=RESOLVED&list_id=2205623&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

Firefox теперь поддерживает Wayland-протокол `xdg_activation_v1`, что означает, что его окна смогут подниматься на передний план при открытии URL-адресов из других приложений, также поддерживающих протокол (Emilio Cobos Álvarez, Firefox 108. [Ссылка](https://bugzilla.mozilla.org/show_bug.cgi?id=1767546))

## Как вы можете помочь

Если вы разработчик, посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2022/11/11/this-week-in-kde-better-environment-variable-support/>  

<!-- 💡: KMenuEdit → Редактор меню KDE -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
