# На этой неделе в KDE: спокойный выпуск Plasma 5.27

На этой неделе мы наконец-то выпустили Plasma 5.27, и пока всё шло без сучка и без задоринки! Сколь-нибудь существенные ухудшения, обнаруженные на данный момент, уже исправлены в корректирующем выпуске, который состоится через несколько дней. Есть ещё некоторое недовольство по поводу новой обводки окон, но всем не угодишь, и, вероятно, мы в итоге сделаем её опциональной.

## Новые возможности

Диспетчер файлов Dolphin теперь позволяет настраивать формат отображения прав файлов в табличном режиме просмотра (Сергей Подтынный, Dolphin 23.04. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/494)):

![0](https://pointieststick.files.wordpress.com/2023/02/image.png)

Со страницы установленного приложения Flatpak в Discover теперь можно сразу перейти к настройкам его прав доступа в Параметрах системы (Иван Ткаченко, Plasma 6.0. [Ссылка 1](https://invent.kde.org/plasma/discover/-/merge_requests/466) и [ссылка 2](https://invent.kde.org/plasma/flatpak-kcm/-/merge_requests/34)):

![1](https://pointieststick.files.wordpress.com/2023/02/image-2.png)

## Улучшения пользовательского интерфейса

Код Dolphin для подсчёта размеров каталогов стал быстрее (Méven Car, Dolphin 23.04. [Ссылка](https://invent.kde.org/system/dolphin/-/merge_requests/511))

Просмотрщик изображений Gwenview теперь масштабирует изображения плавно, а не пошагово, при прокрутке с помощью сенсорной панели с зажатой клавишей Ctrl (Frisco Smit, Gwenview 23.04. [Ссылка](https://invent.kde.org/graphics/gwenview/-/merge_requests/185))

Календари праздников больше не содержат астрономические события, и если у вас дополнительно включён модуль календаря с астрономическими событиями, вы больше не будете видеть одни и те же события дважды (Nate Graham, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465539))

Поиск приложений в диалоговом окне «Открыть с помощью» из портала XDG теперь выполняется по всем приложениям, а не только по ограниченному набору «рекомендуемых» приложений, видимых по умолчанию (Nate Graham, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464521))

Приложениям, использующим портал для запроса разрешения на запись или демонстрацию экрана, теперь можно предоставить некоторую прямоугольную область экрана, а не только весь экран или отдельное окно (Dominique Hummel, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464147))

[3](3_region-recording.mp4)

Пункт «Закрыть» контекстного меню панели задач теперь для ясности меняет название на «Закрыть все» при щелчке по сгруппированным задачам (Fushan Wen, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1382))

Всплывающая подсказка виджета «Прогноз погоды» теперь по умолчанию содержит скорость ветра и влажность (Guilherme Marçal Silva, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/331)):

![2](https://pointieststick.files.wordpress.com/2023/02/image-1-1.png)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Поставщик сведений о погоде Deutscher Wetterdienst (DWD) снова работает после изменения на его стороне формата данных (Emily Ehlert, Plasma 5.24.8. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2632))

Исправлена ошибка в версии 5.27, которая при определённых обстоятельствах могла привести к исчезновению значков на рабочем столе после выхода системы из ждущего режима и до перезапуска Plasma (Marco Martin, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465536))

Исправлена ошибка в версии 5.27, из-за которой к приложениям на основе Electron, использующим слой совместимости XWayland (например, VSCode, Discord и Element), не применялся глобальный коэффициент масштабирования (Nate Graham, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465733))

Новая страница настройки прав доступа приложений Flatpak в Параметрах системы теперь правильно создаёт переопределения для конкретных приложений при использовании системы на языке, отличном от английского (Harald Sitter, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465818))

Исправлена ошибка, из-за которой Plasma могла аварийно завершить работу после выхода из ждущего режима, если с момента перехода в ждущий режим изменился набор подключённых экранов (Marco Martin, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465892))

Исправлена ошибка в версии 5.27, из-за которой всплывающая подсказка виджета «Цифровые часы» напрасно отображала текущее время и часовой пояс, даже если дополнительные часовые пояса не настроены (Nate Graham, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465873))

Виджет «Сети» больше не будет без необходимости отображать петлевой интерфейс (Loopback) при использовании NetworkManager версии 1.42 (David Redondo, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=465655))

Заработала установка верхнего предела заряда для батарей, которые поддерживают ограничение сверху, но не снизу (Fabian Vogt, Plasma 5.27.1. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=464535))

В сеансе Plasma Wayland окна приложений KDE снова правильно запоминают свой размер при использовании нескольких экранов (Nate Graham, Frameworks 5.104. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460260))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Известно о 12 ошибках в Plasma с очень высоким приоритетом (на прошлой неделе было лишь 8). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma стало 43 (на прошлой неделе было 37). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 106 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-02-10&chfieldto=2023-02-17&chfieldvalue=RESOLVED&list_id=2278541&query_format=advanced&resolution=FIXED)

## Изменения вне KDE, затрагивающие KDE

В сеансе Plasma Wayland неполноэкранные веб-приложения Chromium больше не поглощают все глобальные комбинации клавиш (Nick Diego Yamane, Chromium 111. [Ссылка](https://bugs.chromium.org/p/chromium/issues/detail?id=1338554))

## Как вы можете помочь

Если вы пользователь, обновитесь до [Plasma 5.27](https://www.kde.org/announcements/plasma/5/5.27.0/)! Если обновление в вашем дистрибутиве не доступно и не предвидится, подумайте о переходе на другой дистрибутив, поставляющий программное обеспечение ближе к графикам разработчиков.

Разработчики, помогите нам исправить [известные проблемы в Plasma 5.27](https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&keywords=regression%2C%20&keywords_type=allwords&list_id=2278517&query_format=advanced&version=5.26.90&version=5.27.0&version=5.27.1&version=5.27.2&version=5.27.3&version=5.27.4&version=5.27.5). Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этих списков — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/02/17/this-week-in-kde-a-smooth-release-of-plasma-5-27/>  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: directory → каталог -->
<!-- 💡: icons → значки -->
<!-- 💡: task manager → панель задач -->
