# На этой неделе в KDE: первые штрихи Plasma 6

Пока мы шлифуем Plasma 5.27, новые функции и изменения пользовательского интерфейса начинают появляться в Plasma 6! Мы также неплохо проредили 15-минутные ошибки. Ништяки для всех!

## Новые возможности

Страница «Приложения по умолчанию» Параметров системы теперь позволяет легко выбрать предпочитаемое приложение для большего числа типов файлов: добавлены параметры «Просмотрщик изображений», «Аудиопроигрыватель», «Видеопроигрыватель», «Текстовый редактор», «Просмотрщик PDF» и «Архиватор» (Méven Car, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1207)):

![0](https://pointieststick.files.wordpress.com/2023/02/default-applications-kcm.jpg)

*Конечно, вы по-прежнему можете точечно переопределить настройки для конкретных форматов из этих обширных сопоставлений на странице «Привязки файлов». Если вы это сделаете, ваши изменения будут отображаться здесь!*

В приложениях на основе библиотеки Kirigami стандартные элементы списка с укороченным текстом теперь отображают всплывающую подсказку с полным текстом при наведении на них (Иван Ткаченко, Frameworks 5.103. [Ссылка](https://invent.kde.org/frameworks/kirigami/-/merge_requests/878))

## Улучшения пользовательского интерфейса

Интерфейс выбора цвета выделения теперь занимает меньше места, что позволит нам в будущем добавить на страницу «Цвета» другие настройки — например, смену цветовой схемы в зависимости от времени суток, [над чем мы уже работаем](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/2567)! (Tanbir Jishan, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1934)):

![1](https://pointieststick.files.wordpress.com/2023/02/more-compact-accent-color-ui.jpg)

Экранное уведомление, появляющееся при переключении между аудиоустройствами, теперь также показывает уровень заряда батареи выбранного аудиоустройства, если у него, конечно, есть батарея и информация о ней доступна (Kai Uwe Broulik, Plasma 6.0. [Ссылка](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/154)):

![2](https://pointieststick.files.wordpress.com/2023/02/audio-osd-with-battery-info.png)

## Заметные исправления

> (Тут перечислены вручную отобранные исправления высокоприоритетных проблем, критических проблем Wayland, крупных ухудшений и т. д.)

Использование комбинации клавиш для закрытия окна во время его перетаскивания больше не приводит к тому, что окно оставляет после себя свою призрачную замершую тень (Marco Martin, Plasma 5.27. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=439332))

Просмотр страницы «Оформление приложений» в Параметрах системы больше не приводит к скачку загрузки на процессор при наличии некоторых сторонних стилей (Fushan Wen, Plasma 6.0. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=417850))

Исправлены две проблемы с размещением всплывающих окон виджетов на панели Plasma, из-за которых всплывающие окна по ошибке могли быть центрированы относительно панели, не занимающей полностью край экрана, или в многомониторных конфигурациях (Niccolò Venerandi, Frameworks 5.103. [Ссылка](https://invent.kde.org/frameworks/plasma-framework/-/merge_requests/689))

В утилите для создания снимков экрана Spectacle исправлено копирование снимка в буфер обмена сразу после создания в сеансе Plasma Wayland (David Redondo, Frameworks 5.103. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=463199))

В программах на основе QtQuick больше нельзя перетаскивать некоторые элементы интерфейса в прокручиваемых представлениях, когда это не имеет смысла, например, в боковых панелях и списках (Marco Martin, Frameworks 5.103. [Ссылка](https://bugs.kde.org/show_bug.cgi?id=460052))

Исправлено множество мелких недочётов в реализации полос прокрутки в программном обеспечении на основе QtQuick (Иван Ткаченко, Frameworks 5.103. [Ссылка](https://invent.kde.org/frameworks/qqc2-desktop-style/-/merge_requests/214))

Другие сведения об ошибках, которые могут вас заинтересовать:

* Остаётся 9 ошибок в Plasma с очень высоким приоритетом (как и на прошлой неделе). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2125490&priority=VHI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-kirigami&product=frameworks-plasma&product=frameworks-qqc2-desktop-style&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=Touchpad-KCM&product=user-manager&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Известных 15-минутных ошибок в Plasma осталось 42 (на прошлой неделе была 51). [Текущий список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&known_name=VHI-priority%20Plasma%20bugs&list_id=2101429&priority=HI&product=Bluedevil&product=Breeze&product=Discover&product=drkonqi&product=frameworks-plasma&product=kactivitymanagerd&product=kde-gtk-config&product=kdeplasma-addons&product=khelpcenter&product=kinfocenter&product=klipper&product=kmenuedit&product=krunner&product=KScreen&product=kscreenlocker&product=ksmserver&product=ksysguard&product=KSystemLog&product=kwayland-integration&product=kwin&product=Plasma%20SDK&product=Plasma%20Vault&product=Plasma%20Workspace%20Wallpapers&product=plasma-disks&product=plasma-integration&product=plasma-nm&product=plasma-pa&product=plasma-simplemenu&product=plasma-systemmonitor&product=plasmashell&product=policykit-kde-agent-1&product=Powerdevil&product=print-manager&product=printer-applet&product=pulseaudio-qt&product=systemsettings&product=xdg-desktop-portal-kde&query_based_on=VHI-priority%20Plasma%20bugs&query_format=advanced)
* Суммарно 108 ошибок было исправлено на этой неделе во всём программном обеспечении KDE. [Полный список ошибок](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=RESOLVED&chfield=bug_status&chfieldfrom=2023-01-27&chfieldto=2023-02-03&chfieldvalue=RESOLVED&list_id=2266510&query_format=advanced&resolution=FIXED)

## Как вы можете помочь

Помогите с тестированием [бета-версии Plasma 5.27](https://community.kde.org/Plasma/Live_Images#Ships_Plasma_5.27_Beta)! Отчёты об ошибках в бета-версии рассматриваются в первую очередь. Если вы разработчик, то можете помочь и непосредственно с устранением недочётов. Также посмотрите нашу инициативу [«15-минутные ошибки»](https://vk.com/@kde_ru-15-min-bugs). Решение проблем из этого списка — простой способ сделать значительный вклад.

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](http://kde.org/fundraisers/yearend2022) [фонду KDE e.V](https://ev.kde.org/).

<!-- Временная ссылка! -->

*Автор:* [Nate Graham](https://pointieststick.com/author/pointieststick/)  
*Перевод:* [Илья Бизяев](https://ilyabiz.com)  
*Источник:* <https://pointieststick.com/2023/02/03/this-week-in-kde-plasma-6-starts-to-take-shape/>  

<!-- 💡: OSD → экранное уведомление -->
<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: screenshot → снимок экрана -->
